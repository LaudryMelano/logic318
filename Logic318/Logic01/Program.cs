﻿namespace Logic01;
public class Program
{
    static void Main(string[] args)
    {
        callMenu();

        Console.WriteLine();
        Console.Write("Press Any Key...");
        Console.ReadKey();
    }
    
    static void callMenu()
    {
        bool loop = true;

        while (loop)
        {
            Console.Clear();
            Console.WriteLine();
            menu();

            Console.WriteLine();
            Console.Write("Apakah anda ingin mengulangi (y/n) : ");
            string pil = Console.ReadLine().ToLower();
            if (pil == "y")
            {
                Console.Clear();
                Console.WriteLine();
                menu();

                Console.WriteLine();
                Console.Write("Apakah anda ingin mengulangi (y/n) : ");
                pil = Console.ReadLine().ToLower();

                if (pil == "n")
                {
                    loop = false;
                    Console.WriteLine();
                    Console.WriteLine("Terimakasih telah berkunjung");
                }
            }
            else if (pil == "n")
            {
                loop = false;
                Console.WriteLine();
                Console.WriteLine("Terimakasih telah berkunjung");
            }
            else
            {
                Console.WriteLine("Pilih Y atau N!");
            }
        }
    }

    static void menu()
    {
        Console.WriteLine("===== QUIZ DAY 1 =====");
        Console.WriteLine();
        Console.WriteLine("1. Luas dan Keliling Lingkaran");
        Console.WriteLine("2. Luas dan Keliling Persegi");
        Console.WriteLine("3. Modulus");
        Console.WriteLine("4. Putung Rokok");
        Console.WriteLine("5. Grade Nilai");
        Console.WriteLine("6. Ganjil Genap");

        Console.WriteLine();
        Console.Write("Pilih Soal : ");
        int opt = int.Parse(Console.ReadLine());

        Console.WriteLine();

        switch (opt)
        {
            case 1:
                konversi();
                break;
            case 2:
                operatorAritmatika();
                break;
            case 3:
                modulus();
                break;
            case 4:
                operatorPenugasan();
                break;
            case 5:
                operatorPerbandingan();
                break;
            case 6:
                operatorLogika();
                break;
            default:
                Console.WriteLine("Masukkan input yang benar!");
                break;
        }
    }
    static void konversi()
    {
        int myInt = 10;
        double myDouble = 5.25;
        bool myBool = true;

        Console.WriteLine("Convert Int to String : " + Convert.ToString(myInt));
        Console.WriteLine("Convert Int to Double: " + Convert.ToDouble(myInt));
        Console.WriteLine("Convert Double to int: " + Convert.ToInt32(myDouble));
        Console.WriteLine("Convert Bool to String : " + Convert.ToString(myBool));
        Console.WriteLine(myInt.ToString());
    }

    static void operatorAritmatika()
    {
        int mangga, apel, hasil = 0;

        Console.WriteLine("Operaor Aritmatika");

        Console.Write("Masukkan jumlah mangga");
        mangga = int.Parse(Console.ReadLine());

        Console.Write("Masukkan jumlah apel");
        apel = int.Parse(Console.ReadLine());

        hasil = mangga + apel;
        Console.WriteLine($"Total dari mangga dan apel adalah {hasil}");
    }

    static void modulus()
    {
        int mangga, apel, hasil = 0;

        Console.WriteLine("Operaor Aritmatika");

        Console.Write("Masukkan jumlah mangga");
        mangga = int.Parse(Console.ReadLine());

        Console.Write("Masukkan jumlah apel");
        apel = int.Parse(Console.ReadLine());

        hasil = mangga % apel;
        Console.WriteLine($"Mod dari mangga dan apel adalah {hasil}");
    }

    static void operatorPenugasan()
    {
        int mangga = 10;
        int apel = 8;

        //mengisi ulang variabel
        mangga = 15;

        Console.WriteLine("Mangga = " + mangga);

        apel += 6;

        Console.WriteLine("Apel = " + apel);
    }

    static void operatorPerbandingan()
    {
        int mangga, apel = 0;
        Console.WriteLine("=== Operator Perbandingan ===");

        Console.Write("Jumlah Mangga : ");
        mangga = int.Parse(Console.ReadLine());

        Console.Write("Jumlah Apel : ");
        apel = int.Parse(Console.ReadLine());

        Console.WriteLine("Hasil Perbandinga: ");
        Console.WriteLine($"Mangga > Apel : {mangga > apel}");
        Console.WriteLine($"Mangga >= Apel : {mangga >= apel}");
        Console.WriteLine($"Mangga < Apel : {mangga < apel}");
        Console.WriteLine($"Mangga <= Apel : {mangga <= apel}");
        Console.WriteLine($"Mangga != Apel : {mangga != apel}");
        Console.WriteLine($"Mangga == Apel : {mangga == apel}");
    }

    static void operatorLogika()
    {
        Console.WriteLine("=== Operator Logika ===");
        Console.Write("Masukkan umur : ");
        int umur = int.Parse(Console.ReadLine());

        Console.Write("Masukkan Password : ");
        string password = Console.ReadLine().ToLower();

        bool isAdult = umur > 18;

        bool isPasswordValid = password == "admin";

        if (isAdult && isPasswordValid)
        {
            Console.WriteLine("Selamat Datang, Anda sudah dewasa dan password valid");
        }
        else if (isAdult && !isPasswordValid)
        {
            Console.WriteLine("Coba lagi, Anda sudah dewasa dan password Invalid");
        }
        else if (!isAdult && isPasswordValid)
        {
            Console.WriteLine("Coba lagi, Anda belum dewasa dan password valid");
        }
        else
        {
            Console.WriteLine("Coba Lagi, Anda belum dewasa dan password Invalid");
        }
    }

    static void ternary()
    {
        Console.WriteLine("=== TERNARY ===");
        int x, y;

        Console.Write("Masukkan nilai X : ");
        x = int.Parse(Console.ReadLine());

        Console.Write("Masukkan nilai Y : ");
        y = int.Parse(Console.ReadLine());

        string result = x > y ? "X lebih besar dari Y" : "Y lebih besar dari X";

        Console.WriteLine(result);
    }

    static void manipulasiString()
    {
        Console.WriteLine("=== Manipulasi String ===");
        Console.Write("Masukkan Kata : ");
        string kata = Console.ReadLine();

        string remove = kata;
        string insert = kata;
        string replace = kata;

        Console.WriteLine($"Panjang karakter dari {kata} = {kata.Length}");
        Console.WriteLine($"ToUpper dari {kata} = {kata.ToUpper()}");
        Console.WriteLine($"ToLower dari {kata} = {kata.ToLower()}");
        Console.WriteLine($"Remove dari {remove} = {remove.Remove(11)}");
        Console.WriteLine($"Insert dari {replace} = {replace.Replace("id", "net")}");
        Console.WriteLine($"Replace dari {insert} = {insert.Insert(5, "BANGET")}");
    }

    static void splitandjoin()
    {
        Console.WriteLine("=== SPLIT & JOIN ===");

        Console.Write("Masukkan Kalimat : ");
        string[] kalimat = Console.ReadLine().Split(" ");

        foreach (string s in kalimat)
        {
            Console.WriteLine(s);
        }

        Console.WriteLine(string.Join(" + ", kalimat));
    }

    static void subString()
    {
        Console.WriteLine("=== SUBSTRING ===");

        Console.Write("Masukkan Kalimat : ");
        string kalimat = Console.ReadLine();

        Console.WriteLine($"String Tahun = {kalimat.Substring(1, 4)}");
        Console.WriteLine($"String Bulan = {kalimat.Substring(5, 2)}");
        Console.WriteLine($"Kode Lokasi = {kalimat.Substring(7, 2)}");
        Console.WriteLine($"Number = {kalimat.Substring(9)}");
    }

    static void soal1()
    {
        Console.WriteLine("+++ Nilai Rata-rata +++");

        Console.Write("Masukkan Nilai Fisika : ");
        int fisika = int.Parse(Console.ReadLine());
        Console.Write("Masukkan Nilai MTK : ");
        int mtk = int.Parse(Console.ReadLine());
        Console.Write("Masukkan Nilai Kimia : ");
        int kimia = int.Parse(Console.ReadLine());

        int hasil = (fisika + mtk + kimia) / 3;

        Console.WriteLine("Nilai Rata-Rata : " + hasil);

        //string res = (hasil>50)?"Selamat\nKamu Berhasil\nKamu Hebat": "Maaf\nkamu gagal";

        if (hasil > 50)
            Console.WriteLine("Selamat\nKamu Berhasil\nKamu Hebat");
        else
            Console.WriteLine("Maaf\nkamu gagal");
    }

    static void soal2()
    {
        Console.WriteLine("\t=== MATAHARI ===");

        Console.Write("\nMasukkan Kode Baju \t: ");
        int kode = int.Parse(Console.ReadLine());

        Console.Write("Masukkan Ukuran Baju \t: ");
        string ukuran = Console.ReadLine().ToLower();

        string merk = "";
        int harga = 0;
        if (kode > 0 && kode < 4)
        {
            if (kode == 1)
            {
                merk = "IMP";
                if (ukuran == "s")
                {
                    harga = 200000;
                }
                else if (ukuran == "m")
                {
                    harga = 220000;
                }
                else
                {
                    harga = 250000;
                }
            }
            else if (kode == 2)
            {
                merk = "Prada";
                if (ukuran == "s")
                {
                    harga = 150000;
                }
                else if (ukuran == "m")
                {
                    harga = 160000;
                }
                else
                {
                    harga = 170000;
                }
            }
            else if (kode == 3)
            {
                merk = "Gucci";
                harga = 200000;
            }
            Console.WriteLine("\nMerk Baju    : " + merk);
            Console.WriteLine("Harga Baju   : " + harga);
        }
        else
        {
            Console.WriteLine("Kode Baju Invalid!");
        }

    }

}


