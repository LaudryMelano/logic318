﻿namespace Logic07
{
    public class Program
    {
        public Program()
        {
            callMenu();
        }
        static void Main(string[] args)
        {
            callMenu();

            Console.WriteLine();
            Console.Write("Press Any Key...");
            Console.ReadKey();
        }

        static void callMenu()
        {
            bool loop = true;

            while (loop)
            {
                Console.Clear();
                Console.WriteLine();
                menu();

                Console.WriteLine();
                Console.WriteLine();
                Console.Write("Apakah anda ingin mengulangi (y/n) : ");
                string pil = Console.ReadLine().ToLower();
                if (pil == "y")
                {
                    Console.Clear();
                    Console.WriteLine();
                    menu();

                    Console.WriteLine();
                    Console.WriteLine();
                    Console.Write("Apakah anda ingin mengulangi (y/n) : ");
                    pil = Console.ReadLine().ToLower();

                    if (pil == "n")
                    {
                        loop = false;
                        Console.WriteLine();
                        Console.WriteLine("Terimakasih telah berkunjung di Logic 06");
                    }
                }
                else if (pil == "n")
                {
                    loop = false;
                    Console.WriteLine();
                    Console.WriteLine("Terimakasih telah berkunjung di Logic 06");
                }
                else
                {
                    Console.WriteLine("Pilih Y atau N!");
                }
            }
        }

        static void menu()
        {
            Console.WriteLine("===== Welcome to Day 07 ======");
            Console.WriteLine();
            Console.WriteLine("=== 1. Camel Case ===");
            Console.WriteLine("=== 2. Time Conversion ===");
            Console.WriteLine("=== 3. Simple Array Sum ===");
            Console.WriteLine("=== 4. Diagonal Difference ===");
            Console.WriteLine("=== 5. Plus Minus ===");
            Console.WriteLine("=== 6. Star Case ===");
            Console.WriteLine("=== 7. Min Max Sum ===");
            Console.WriteLine("=== 8. Candle Birthday ===");
            Console.WriteLine("=== 9. Compare the triplets ===");
            Console.WriteLine();
            Console.Write("Masukkan no soal : ");

            int soal = int.Parse(Console.ReadLine());

            switch (soal)
            {
                case 1:
                    CamelCase camelCase = new CamelCase();
                    break;
                case 2:
                    
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    break;
                case 6:
                    break;
                case 7:
                    break;
                case 8:
                    break;
                case 9:
                    break;
                case 10:
                    break;
                default:
                    break;
            }
        }
    }
}