﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    public class CamelCase
    {
        public CamelCase()
        {
            int  count = 0;

            Console.Write("Masukkan Kalimat : ");
            char[] kal = Console.ReadLine().ToCharArray();

            //string tmp = "";
            for (int i = 0; i < kal.Length; i++)
            {
                if (i == 0 || Char.IsUpper(kal[i]))
                {
                    Console.WriteLine();
                    count++;
                }
                Console.Write(kal[i]);
            }
            Console.WriteLine();
            Console.WriteLine(count);
        }
    }
}
