﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    public class SearchString
    {
        public SearchString()
        {
            //Laudry
            // ABCDEFGHIJKLMNOPQRSTUVWXYZ
            Console.WriteLine("====== Search String ======");

            Console.Write("Masukkan Kata : ");
            string kata = Console.ReadLine();

            string libString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            for (int i = 0; i < kata.Length; i++)
            {
                Console.WriteLine($"Index string : {kata[i]}, {libString.IndexOf(kata[i])}");
            }
        }
    }
}
