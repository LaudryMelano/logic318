﻿
//soal1();
//soal2();
//soal3();
//soal4();
//soal5();
//soal6();

callMenu();

static void callMenu()
{
    bool loop = true;

    while (loop)
    {
        Console.Clear();
        Console.WriteLine();
        menu();

        Console.WriteLine();
        Console.Write("Apakah anda ingin mengulangi (y/n) : ");
        string pil = Console.ReadLine().ToLower();
        if (pil == "y")
        {
            Console.Clear();
            Console.WriteLine();
            menu();

            Console.WriteLine();
            Console.Write("Apakah anda ingin mengulangi (y/n) : ");
            pil = Console.ReadLine().ToLower();

            if(pil == "n")
            {
                loop = false;
                Console.WriteLine();
                Console.WriteLine("Terimakasih telah berkunjung");
            }
        }
        else if(pil == "n")
        {
            loop = false;
            Console.WriteLine();
            Console.WriteLine("Terimakasih telah berkunjung");
        }
        else
        {
            Console.WriteLine("Pilih Y atau N!");
        }
    }
}

static void menu()
{ 
    Console.WriteLine("===== QUIZ DAY 1 =====");
    Console.WriteLine();
    Console.WriteLine("1. Luas dan Keliling Lingkaran");
    Console.WriteLine("2. Luas dan Keliling Persegi");
    Console.WriteLine("3. Modulus");
    Console.WriteLine("4. Putung Rokok");
    Console.WriteLine("5. Grade Nilai");
    Console.WriteLine("6. Ganjil Genap");

    Console.WriteLine();
    Console.Write("Pilih Soal : ");
    int opt = int.Parse(Console.ReadLine());

    Console.WriteLine();

    switch (opt)
    {
        case 1:
            soal1();
            break;
        case 2: 
            soal2();
            break;
        case 3:
            soal3();
            break;
        case 4:
            soal4();
            break;
        case 5:
            soal5();
            break;
        case 6:
            soal6();
            break;
        default:
            Console.WriteLine("Masukkan input yang benar!");
            break;
    }

}

static void soal1()
{
    Console.WriteLine("=== Luas & Keliling Lingkaran");
    bool loop = true;
    while (loop)
    {
        Console.Write("Masukkan Jari-Jari : ");
        int r = int.Parse(Console.ReadLine());

        if (r > 0)
        {
            double luas = 22.0/7 * r * r;
            double keliling = 2 * 22.0/7 * r;

            Console.WriteLine("Luas Lingkaran : " + luas + " cm\xb2");
            Console.WriteLine("Keliling Lingkaran : " + keliling +"cm");
            loop= false;
        }
        else
        {
            Console.WriteLine("Input jari-jari yang sesuai!");
        }
    }
    
    
}

static void soal2()
{
    Console.WriteLine("=== Luas & keliling Persegi");

    bool loop = true;

    while (loop)
    {
        Console.Write("Masukan sisi : ");
        int s = int.Parse(Console.ReadLine());

        if(s>0)
        {
            int luas = s * s;
            int keliling = 4 * s;

            Console.WriteLine("Luas Persegi : " + luas);
            Console.WriteLine("Keliling Persegi : " + keliling);
            loop= false;
        }
        else
        {
            Console.WriteLine("Input Sisi yang sesuai!");
        }
    }
}

static void soal3()
{
    Console.WriteLine("=== Modulus ===");
    Console.Write("Masukkan Angka : ");
    int angka = int.Parse(Console.ReadLine());

    Console.Write("Masukkan Pembagi : ");
    int pembagi = int.Parse(Console.ReadLine());

    int result = angka % pembagi;

    if (result == 0)
    {
        Console.WriteLine($"Angka {angka} % {pembagi} adalah {result}");
    }
    else
    {
        Console.WriteLine($"Angka {angka} % {pembagi} bukan 0 melainkan {result}");
    }
}

static void soal4()
{
    Console.WriteLine("==== Puntung Rokok =====");

    Console.Write("Masukkan jumlah putung : ");
    int putung = int.Parse(Console.ReadLine());

    int rokok = putung / 8;
    int penghasilan = rokok * 500;
    int sisa = putung % 8;

    Console.WriteLine("Batang Rokok yang dihasilkan : " + rokok);
    Console.WriteLine("Penghasilan yang didapatkan : " + penghasilan);
    Console.WriteLine("Puntung yang tersisa : " + sisa);
}

static void soal5()
{
    Console.WriteLine("\t==== GRADE NILAI ====");

    Console.WriteLine();
    Console.Write("Masukkan nilai mahasiswa : ");
    int nilai = int.Parse(Console.ReadLine());
    Console.WriteLine();

    if (nilai >= 80 && nilai <= 100)
        Console.WriteLine("Nilai mahasiswa \t: A" );
    else if (nilai >= 60 && nilai < 80)
        Console.WriteLine("Nilai mahasiswa \t: B");
    else if (nilai >= 0 && nilai <60)
        Console.WriteLine("Nilai mahasiswa \t: C");
    else
        Console.WriteLine("Masukkan input nilai yang sesuai");
}

static void soal6()
{
    Console.WriteLine("==== Bilangan Ganjil ====");

    bool loop = true;
    while (loop)
    {
        Console.Write("Masukkan angka : ");
        int angka = int.Parse(Console.ReadLine());
        if (angka >= 0) 
        {
            if (angka % 2 == 0)
            {
                Console.WriteLine($"Angka {angka} adalah genap");
            }
            else
            {
                Console.WriteLine($"Angka {angka} adalah ganjil");
            }
        }
        else
        {
            Console.WriteLine("Masukkan Angka yang benar!");
        }       
    }
}