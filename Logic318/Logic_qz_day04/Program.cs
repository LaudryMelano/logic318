﻿
soal4();

static void soal1()
{
    Console.WriteLine("====== GAJI KARYAWAN =======");
    Console.WriteLine();

    Console.Write("Masukkan Golongan \t: ");
    string gol =Console.ReadLine();

    Console.Write("Masukkan Total Jam Kerja \t: ");
    double jamKerja = double.Parse(Console.ReadLine());

    double upah=0, lembur = 0, total=0;
    int gol1 = 2000, gol2=3000, gol3= 4000, gol4=5000;

    Console.WriteLine();
    if(jamKerja >40)
    {
        if (gol == "1")
        {
            upah = gol1 * 40;
            lembur = 1.5 * (jamKerja - 40) * gol1;
        }else if(gol == "2")
        {
            upah = gol2 * 40;
            lembur = 1.5 * (jamKerja - 40) * gol2;
        }else if(gol == "3")
        {
            upah = gol3 * 40;
            lembur = 1.5 * (jamKerja - 40) * gol3;
        }else if(gol == "4")
        {
            upah = gol4 * 40;
            lembur = 1.5 * (jamKerja - 40) * gol4;
        }

        total = upah + lembur;
    }
    else
    {
        if (gol == "1")
        {
            upah = gol1 * jamKerja;
        }
        else if (gol == "2")
        {
            upah = gol2 * jamKerja;
        }
        else if (gol == "3")
        {
            upah = gol3 * jamKerja;
        }
        else if (gol == "4")
        {
            upah = gol4 * jamKerja;
        }

        total = upah;
    }
    //Angka 9 untuk format rata kanan rata kanan 
    Console.WriteLine($"Upah    : Rp.{upah, 9}" );
    Console.WriteLine("Lembur  : Rp." + lembur);
    Console.WriteLine("Total   : Rp." + total);
    
}

static void soal2()
{
    Console.WriteLine("====== SPLIT KALIMAT =======");
    Console.WriteLine();

    Console.Write("Masukkan Kalimat : " );
    string[] kal = Console.ReadLine().Split(" ");

    for (int i = 0; i < kal.Length; i++)
    {
        Console.WriteLine($"Kata {i} = {kal[i]}");
    }

    Console.WriteLine("Total Kata adalah " + kal.Length);
}

static void soal3()
{
    Console.WriteLine("====== Replace Kalimat =======");
    Console.WriteLine();

    Console.Write("Masukkan Kalimat : ");
    string[] kal = Console.ReadLine().Split();
    string tmp = "";

    for (int i = 0; i < kal.Length; i++)
    {
        for (int j = 0; j < kal[i].Length; j++)
        {
            if (j == 0)
            {
                tmp += kal[i][j];
            } else if (j == kal[i].Length - 1)
            {
                tmp += kal[i][j];
            }
            else
            {
                tmp += kal[i].Replace(kal[i], "*");
            }
        }
        tmp += " ";
    }

    Console.WriteLine(tmp);
}

static void soal4()
{
    Console.WriteLine("====== Replace Kalimat 2 =======");
    Console.WriteLine();

    Console.Write("Masukkan Kalimat : ");
    string[] kal = Console.ReadLine().Split();
    string tmp = "";

    for (int i = 0; i < kal.Length; i++)
    {
        for (int j = 0; j < kal[i].Length; j++)
        {
            if (j == 0)
            {
                tmp += kal[i].Replace(kal[i], "*");
            }
            else if (j == kal[i].Length - 1)
            {
                tmp += kal[i].Replace(kal[i], "*");
            }
            else
            {
                tmp += kal[i][j];
            }
        }
        tmp += " ";
    }

    Console.WriteLine(tmp);
}

static void soal5()
{
    Console.WriteLine("====== Remove Kalimat =======");
    Console.WriteLine();

    Console.Write("Masukkan Kalimat : ");
    string[] kal = Console.ReadLine().Split();
    string tmp = "";

    for (int i = 0; i < kal.Length; i++)
    {
        for (int j = 0; j < kal[i].Length; j++)
        {
            if (j == 0)
            {
                tmp += kal[i].Remove(0);
            }
            else
            {
                tmp += kal[i][j];
            }
        }
        tmp += " ";
    }

    Console.WriteLine(tmp);

}

static void soal6()
{
    Console.WriteLine("======= KELIPATAN 3 ========");
    Console.WriteLine();

    Console.Write("Masukkan angka N : ");
    int n = int.Parse(Console.ReadLine());
    int count = 3;

    string tmp = "";

    for (int i = 1; i <= n; i++)
    {
        if (i % 2 == 0)
            tmp += " * ";
        else
            tmp += count;

        count *= 3;
    }

    Console.WriteLine(tmp);
}

static void soal7()
{
    Console.WriteLine("======= Fibonacci ========");
    Console.WriteLine();

    Console.Write("Masukkan angka N : ");
    int n = int.Parse(Console.ReadLine());

    int[] numArr = new int[n];

    for (int i = 0; i < n; i++)
    {
        if (i <= 1)
        {
            numArr[i] = 1;
        }
        else
        {
            numArr[i] = numArr[i - 1] + numArr[i-2];
        }
    }

    Console.Write(String.Join(",", numArr));
   
}

static void soal8()
{
    Console.WriteLine("======= ANGKAAAA ========");
    Console.WriteLine();

    Console.Write("Masukkan angka N : ");
    int n = int.Parse(Console.ReadLine());

    int n1 = 1, n2=n;
    string tmp = "";

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (i == 0)
            {
                tmp += (n1++)+" ";               
            }else if (i == n - 1)
            {
                tmp += (n2--)+" ";                
            }
            else
            {
                if(j==0 || j==n-1)
                    tmp += "* ";
                else
                    tmp += "  ";
            }
        }
        tmp += "\n";
    }

    Console.WriteLine();
    Console.WriteLine(tmp);
}