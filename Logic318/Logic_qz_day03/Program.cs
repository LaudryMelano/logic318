﻿//soal1();
//soal3();
//soal4();
//soal6();
soal7();
static void soal1()
{
    Console.WriteLine("===== GRADE NILAI MAHASISWA =====");
    Console.WriteLine();
    loop:
    try
    {
        Console.Write("Masukkan Nilai Mahasiswa : ");
        int nilai = int.Parse(Console.ReadLine());

        if (nilai >= 90 && nilai <= 100)
            Console.WriteLine("A");
        else if (nilai >= 70 && nilai < 90)
            Console.WriteLine("B");
        else if (nilai >= 50 && nilai < 70)
            Console.WriteLine("C");
        else if (nilai >= 0 && nilai < 50)
            Console.WriteLine("E");
        else
            Console.WriteLine("Masukkan input nilai yang benar");
    }
    catch
    {
        Console.WriteLine("Nilai Tidak Boleh Koseng");
        goto loop;
    }

    
}

static void soal2()
{
    Console.WriteLine("====== BLESSING CELL ======");
    Console.WriteLine();

    Console.Write("Masukkan Nominal Pulsa : ");
    int pulsa = int.Parse(Console.ReadLine());

    int poin = 0;
    if (pulsa > 0)
    {
        if (pulsa >= 10000 && pulsa < 25000)
        {
            poin = 80;
        }
        else if (pulsa >= 25000 && pulsa < 50000)
        {
            poin = 200;
        }
        else if (pulsa >= 50000 && pulsa < 100000)
        {
            poin = 400;
        }
        else if (pulsa >= 100000)
        {
            poin = 800;
        }
        else
        {
            Console.WriteLine("Maaf nominal pulsa yang anda beli belum mendapatkan poin");
        }

        Console.WriteLine("Pulsa yang anda beli " + pulsa);
        Console.WriteLine("Poin yang anda dapatkan adalah " + poin);
    }
    else
    {
        Console.WriteLine("Masukkan nominal pulsa yang benar!");
    }
   
}

static void soal3()
{
    Console.WriteLine("===== GRAB POOD ======");
    Console.WriteLine();

    Console.Write("Masukkan nominal Belanja : ");
    int belanja = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Jarak : ");
    double jarak = double.Parse(Console.ReadLine());
    Console.Write("Masukkan Kode Promo : ");
    string promo = Console.ReadLine().ToUpper();

    int ongkir = 5000;
    double totalOngkir = 0;
    double diskon = 0;
    double totalBelanja = 0;
    int maxDiskon = 30000;

    if (belanja >= 30000 && promo == "JKTOVO")
    {
        if (jarak > 5)
        {         
            jarak = Math.Round(jarak - 5, MidpointRounding.ToPositiveInfinity);
            totalOngkir = ongkir + (jarak * 1000);
        }
        else
        {
            totalOngkir = ongkir;
        }
        diskon = belanja * 0.4;
        if (diskon > maxDiskon)
        {
            diskon = maxDiskon;
        }
       
        totalBelanja = belanja - diskon + totalOngkir;
    }
    else 
    {
        if (jarak > 5)
        {

            jarak = jarak -  5;
            totalOngkir = ongkir + (jarak * 1000);
        }
        else
        {
            totalOngkir = ongkir;
        }
        
        totalBelanja = belanja + totalOngkir;
    }

    Console.WriteLine();
    Console.WriteLine("======= INVOICE =======");
    Console.WriteLine($"Belanja                 : Rp.{belanja}");
    Console.WriteLine($"Diskon 40% (Max.30rb)   : Rp.{diskon}");
    Console.WriteLine($"Ongkir                  : Rp.{totalOngkir}");
    Console.WriteLine($"Total Belanja           : Rp.{totalBelanja}");
}

static void soal4()
{
    Console.WriteLine("\t\t====== SHOPAI POOOD =======");
    Console.WriteLine();
    Console.WriteLine("1. Min Order 30rb free ongkir 5rb dan potongan harga belanja 5rb");
    Console.WriteLine("2. Min Order 50rb free ongkir 10rb dan potongan harga belanja 10rb");
    Console.WriteLine("3. Min Order 100rb free ongkir 20rb dan potongan harga belanja 10rb");
    Console.WriteLine();

    Console.Write("Masukkan Nominal belanja \t: Rp.");
    int belanja = int.Parse(Console.ReadLine());

    Console.Write("Masukkan Ongkos Kirim \t\t: Rp.");
    int ongkir = int.Parse(Console.ReadLine());

    int totalBelanja=0, diskonOngkir=0, diskonBelanja =0;

    if (belanja >= 30000)
    {
    loop:
        Console.WriteLine();
        Console.Write("Pilih Voucher : ");
        string voucher = Console.ReadLine();

        if (voucher != "")
        {
            if ((belanja >= 30000 && belanja < 50000))
            {
                if (voucher == "1")
                {
                    diskonOngkir = 5000;
                    diskonBelanja = 5000;
                }
                else
                {
                    Console.WriteLine("Pilih voucher yang sesuai dengan nominal belanja Anda!");
                    goto loop;
                }
            }
            else if ((belanja >= 50000 && belanja < 100000))
            {
                if (voucher == "1")
                {
                    diskonOngkir = 5000;
                    diskonBelanja = 5000;
                } else if (voucher == "2")
                {
                    diskonOngkir = 10000;
                    diskonBelanja = 10000;
                }
                else
                {
                    Console.WriteLine("Pilih voucher yang sesuai dengan nominal belanja Anda!");
                    goto loop;
                }
            }
            else if (belanja >= 100000)
            {
                if (voucher == "1")
                {
                    diskonOngkir = 5000;
                    diskonBelanja = 5000;
                }
                else if (voucher == "2")
                {
                    diskonOngkir = 10000;
                    diskonBelanja = 10000;
                }
                else if(voucher=="3")
                { 
                    diskonOngkir = 20000;
                    diskonBelanja = 10000;
                }
                else
                {
                    Console.WriteLine("Pilih voucher yang sesuai dengan nominal belanja Anda!");
                    goto loop;
                }
            }

            if (ongkir <= diskonOngkir)
            {
                totalBelanja = belanja - diskonBelanja;
            }
            else
            {
                totalBelanja = (belanja+ongkir) - diskonBelanja - diskonOngkir;
            }
        }
        else
        {
            totalBelanja = belanja + ongkir;
        }
    }
    else
    {
        totalBelanja = belanja + ongkir;
    }

    Console.Write("\n");
    Console.WriteLine("\t======= INVOICE =======");
    Console.WriteLine();
    Console.WriteLine($"Belanja                 : Rp.{belanja}");
    Console.WriteLine($"Ongkir                  : Rp.{ongkir}");
    Console.WriteLine($"Diskon Ongkir           : Rp.{diskonOngkir}");
    Console.WriteLine($"Diskon Belanja          : Rp.{diskonBelanja}");
    Console.WriteLine($"Total Belanja           : Rp.{totalBelanja}");
}

static void soal5()
{
    Console.WriteLine("====== CEK GENERASI ======");
    Console.WriteLine();

    Console.Write("Masukkan Nama Anda : ");
    string nama = Console.ReadLine();

    Console.WriteLine("Tahun Berapa Anda Lahir : ");
    int tahun = int.Parse(Console.ReadLine());

    string gol = "";

    if(tahun >= 1944 &&  tahun <= 1966)
    {
        gol = "Baby Boomer";
    }else if(tahun >=1965 && tahun <= 1979)
    {
        gol = "Generasi X";
    }else if(tahun >=1980 && tahun <= 1994)
    {
        gol = "Generasi Y (Milenials)";
    }else if(tahun >= 1995 &&  tahun <= 2015)
    {
        gol = "Generasi Z";
    }
    else
    {
        Console.WriteLine("Maaf Tahun lahir anda belum terdaftar di golongan pada aplikasi kami");
    }

    Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong {gol}");
}

static void soal6()
{
    Console.WriteLine("===== GAJI KARYAWAN =====");
    Console.WriteLine();

    Console.Write("Masukkan Nama : ");
    string nama = Console.ReadLine();

    Console.Write("Masukkan Tunjangan : Rp.");
    double tunjangan = double.Parse(Console.ReadLine()); 
    
    Console.Write("Masukkan Gaji Pokok : Rp.");
    double gaji = double.Parse(Console.ReadLine());

    Console.Write("Banyaknya Bulan : ");
    int bulan = int.Parse(Console.ReadLine());

    double pajak=0,bpjs,gajiPerbulan,totalGaji,totalPajak;

   
    if (gaji + tunjangan <= 5000000)
        pajak = 0.05;
    else if (gaji + tunjangan > 5000000)
        pajak = 0.1;
    else if(gaji + tunjangan > 10000000)
        pajak = 0.15;
    else
        Console.WriteLine("Masukkan Data yang benar!");

    if(gaji>0 && tunjangan>0 && bulan > 0)
    {
        totalPajak = pajak * (gaji + tunjangan);
        bpjs = 0.03 * (gaji + tunjangan);
        gajiPerbulan = (gaji + tunjangan) - (bpjs + totalPajak);
        totalGaji = ((gaji + tunjangan) - (totalPajak + bpjs)) * bulan;

        Console.WriteLine();
        Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut :");
        Console.WriteLine($"Pajak                     : Rp.{totalPajak}");
        Console.WriteLine($"BPJS                      : Rp.{bpjs}");
        Console.WriteLine($"Gaji perBulan             : Rp.{gajiPerbulan}");
        Console.WriteLine($"Total Gaji/Banyak bulan   : Rp.{totalGaji}");
    }
    else
    {
        Console.WriteLine("Harap Masukkan Data yang benar!");
    }
  
}

static void soal7() 
{
    Console.WriteLine("==== Selamat Datang Di Aplikasi BMI ====");
    Console.WriteLine();

    double tbM;
    double bmi;
    string result = default;

    Console.Write("Masukkan Berat Badan Anda (kg): ");
    int bb = int.Parse(Console.ReadLine());

    Console.Write("Masukkan Tinggi Badan Anda (cm): ");
    double tbCm = double.Parse(Console.ReadLine());

    Console.WriteLine();

    tbM = tbCm / 100;

    bmi = bb / Math.Pow(tbM, 2);

    if (tbCm > 0)
    {
        if (bmi > 0 && bmi < 18.5)
        {
            result = "Kurus";
        }
        else if (bmi >= 18.5 && bmi < 25)
        {
            result = "Diet";
        }
        else if (bmi >= 25)
        {
            result = "Obesitas";
        }
        else
        {
            Console.WriteLine("Masukkan input angka yang sesuai!");
        }

        Console.WriteLine($"Anda termasuk berbadan {result}");
    }
    else
    {
        Console.WriteLine("Masukkan input angka yang sesuai!");
    }

    

}

static void soal8()
{
    Console.WriteLine("==== Selamat Datang Di Aplikasi Mean ====");
    Console.WriteLine();

    double mean;

    Console.Write("Masukkan Nilai MTK       : ");
    double mtk = double.Parse(Console.ReadLine());

    Console.Write("Masukkan Nilai FISIKA    : ");
    double fisika = double.Parse(Console.ReadLine());

    Console.Write("Masukkan Nilai KIMIA     : ");
    double kimia = double.Parse(Console.ReadLine());

    Console.WriteLine();

    mean = (mtk + fisika + kimia) / 3;

    Console.WriteLine();

    if (mtk > 0 && fisika > 0 && kimia > 0)
    {
        Console.WriteLine($"Nilai Rata-rata : {Math.Round(mean, 2)}");
        if (mean >= 50 && mean <= 100)
        {
            Console.WriteLine("Selamat\nKamu Berhasil\nKamu Hebat");
        }
        else if (mean >= 0 && mean < 50)
        {
            Console.WriteLine("Maaf\nKamu Gagal");
        }
        else
        {
            Console.WriteLine("Pastikan Nilai yang Anda Masukkan sudah benar!");
        }
    }
    else
    {
        Console.WriteLine("Pastikan Nilai yang Anda Masukkan sudah benar!");
    }
}
