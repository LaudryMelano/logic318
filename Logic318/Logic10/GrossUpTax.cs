﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic10
{
    public class GrossUpTax
    {
        public GrossUpTax()
        {
            Console.WriteLine();
            Console.Write("====== Gross Up Tax ======");
            Console.WriteLine();

            Console.Write("Masukkan Gaji Anda : Rp.");
            double gaji = int.Parse(Console.ReadLine());

            double taxP = 0;
            if(gaji > 25000)
            {
                double result = GrossUpTaxCount(gaji, taxP);
                Console.WriteLine();
                Console.WriteLine(result);
            }
            else
                Console.WriteLine("Anda bebas pajak");

        }

        public double GrossUpTaxCount(double gaji, double taxP)
        {
            
            double tax = 0, min =0;
            double totalGaji = gaji + taxP;

            min = totalGaji - 25000;
            if (min > 0 && min < 50000)
            {
                tax = Math.Round(min * 0.05, MidpointRounding.ToNegativeInfinity);
            }
            else if (min > 50000 && min <= 100000)
            {
                tax = Math.Round(2500 + (min-50000) * 0.1, MidpointRounding.ToNegativeInfinity);
            }
            else if (min > 100000 && min <= 200000)
            {
                tax = Math.Round(7500 + (min-100000) * 0.15, MidpointRounding.ToNegativeInfinity);
            }
            else
            {
                tax = Math.Round(22500 + (min-200000) * 0.25, MidpointRounding.ToNegativeInfinity);
            }

            if(tax == taxP)
            {
                return tax;
            }

            taxP = tax;

            return GrossUpTaxCount(gaji, taxP);
        }
    }
}
