﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic10
{
    public class Cheese
    {
        public Cheese()
        {
            Console.WriteLine();
            Console.WriteLine("======= Simplified Chess Engine =======");
            Console.WriteLine();

            //Console.Write("Massukan jumlah putih : ");
            //int putih = int.Parse(Console.ReadLine());

            //Console.Write("Massukan jumlah hitam : ");
            //int hitam = int.Parse(Console.ReadLine());

            //Console.Write("Massukan jumlah jalan : ");
            //int jalan = int.Parse(Console.ReadLine());

            //for (int i = 0; i < hitam+putih; i++)
            //{
            //    char[] game = Console.ReadLine().ToUpper().ToCharArray();
            //}

            Console.Write("Massukan queen white : ");
            string qw = Console.ReadLine();

            Console.Write("Massukan queen black : ");
            string qb = Console.ReadLine();

            string[,] cBoard = new string[4, 4];
            string rowMap = "4321";
            string colMap = "ABCD";

            string tmp = "";

            for (int i = 0; i < cBoard.GetLength(0); i++)
            {
                for (int j = 0; j < cBoard.GetLength(1); j++)
                {
                    if (i == rowMap.IndexOf(qw[1]) || j == colMap.IndexOf(qw[0])
                        || rowMap.IndexOf(qw[1]) + j == colMap.IndexOf(qw[0]) + i
                        || rowMap.IndexOf(qw[1]) + colMap.IndexOf(qw[0]) == i + j)
                    {
                        cBoard[i, j] = "X";
                    }
                    else
                    {
                        cBoard[i, j] = "*";
                    }
                }
            }

            if (cBoard[rowMap.IndexOf(qb[1]), colMap.IndexOf(qb[0])] == "X")
                Console.WriteLine("Yes");
            else
                Console.WriteLine("No");

            cBoard[rowMap.IndexOf(qw[1]), colMap.IndexOf(qw[0])] = "QW";
            cBoard[rowMap.IndexOf(qb[1]), colMap.IndexOf(qb[0])] = "QB";

            Printing.Array2Dim(cBoard);
            Console.WriteLine(tmp);
        }
    }
}
