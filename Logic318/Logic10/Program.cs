﻿

namespace Logic10
{
    public class Program
    {
        public Program()
        {
            callMenu();
        }
        static void Main(string[] args)
        {
            callMenu();

            Console.WriteLine();
            Console.Write("Press Any Key...");
            Console.ReadKey();
        }

        static void callMenu()
        {
            bool loop = true;

            while (loop)
            {
                Console.Clear();
                Console.WriteLine();
                menu();

                Console.WriteLine();
                Console.WriteLine();
                Console.Write("Apakah anda ingin mengulangi (y/n) : ");
                string pil = Console.ReadLine().ToLower();
                if (pil == "y")
                {
                    Console.Clear();
                    Console.WriteLine();
                    menu();

                    Console.WriteLine();
                    Console.WriteLine();
                    Console.Write("Apakah anda ingin mengulangi (y/n) : ");
                    pil = Console.ReadLine().ToLower();

                    if (pil == "n")
                    {
                        loop = false;
                        Console.WriteLine();
                        Console.WriteLine("Terimakasih telah berkunjung di Logic 10");
                    }
                }
                else if (pil == "n")
                {
                    loop = false;
                    Console.WriteLine();
                    Console.WriteLine("Terimakasih telah berkunjung di Logic 10");
                }
                else
                {
                    Console.WriteLine("Pilih Y atau N!");
                }
            }
        }

        static void menu()
        {
            Console.WriteLine("===== Welcome to Logic10 ======");
            Console.WriteLine();
            Console.WriteLine("=== 1. Cheese ===");
            Console.WriteLine("=== 2. Prime Level Up ===");
            Console.WriteLine("=== 3. Progressive Tax ===");
            Console.WriteLine("=== 4. Gross Up Tax ===");
            Console.WriteLine("=== 5.  ===");
            Console.WriteLine("=== 6. ===");
            Console.WriteLine("=== 7. ===");

            Console.WriteLine();
            Console.Write("Masukkan no soal : ");

            int soal = int.Parse(Console.ReadLine());

            switch (soal)
            {
                case 1:
                    Cheese cheese = new Cheese();
                    break;
                case 2:
                    PrimeLevelUp primeLevelUp = new PrimeLevelUp();
                    break;
                case 3:
                    ProgressiveTax progressiveTax = new ProgressiveTax();
                    break;
                case 4:
                    GrossUpTax grossUp = new GrossUpTax();  
                    break;
                case 5:
                    break;
                case 6:
                    break;
                case 7:
                    break;
                default:
                    break;
            }
        }
    }
}
