﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic10
{
    public class PrimeLevelUp
    {
        public PrimeLevelUp()
        {
            Console.WriteLine("======= PRIME LEVEL UP ======");

            Console.Write("Masukkan n : ");
            int n = int.Parse(Console.ReadLine());

            int[] tmp = new int[n];
            int count = 0, prima=2, index=0;
            while (count < n)
            {
            bool prime = true;
                for (int i = 2; i < prima; i++)
                {
                    if (prima % i == 0)
                    {
                        prime = false;
                        break;
                    }
                }

                if (prime)
                {
                    tmp[index] = prima;
                    count++;
                    index++;
                }

                prima++;
            }
            Console.WriteLine(String.Join(",", tmp));

            //public PrimeLevelUp()
            //{
            //    Console.WriteLine("======= PRIME LEVEL UP ======");

            //    Console.Write("Masukkan n : ");
            //    int n = int.Parse(Console.ReadLine());

            //    for (int i = 2; i < n + 2; i++)
            //    {
            //        if (PrimeCheck(i))
            //            Console.Write($"{i} ");
            //        else
            //            n++;
            //    }
            //}

            //static bool PrimeCheck(int n)
            //{
            //    for (int i = 2; i < n; i++)
            //    {
            //        if (n % i == 0)
            //            return false;
            //    }
            //    return true;
            //}
        }


    }
}
