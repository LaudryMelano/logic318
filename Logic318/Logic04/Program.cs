﻿using Logic04;

//abstractClass();
//objectClass();
//construcor();
//encasulaption();
//inheritance();
//overriding();
//returnTypePenjumlahan();
//rekrusifDesc();

static void rekrusifAsc()
{
    Console.WriteLine("==== Rekrusif Function ====");
    Console.WriteLine("Masukkan input : ");
    int input = int.Parse(Console.ReadLine());
    int start = 1;

    perulanganAsc(input, start);
}

static int perulanganAsc(int input, int start)
{
    if (input == start)
    {
        Console.WriteLine($"{start}");
        return start;
    }

    Console.WriteLine($"{start}");
    return perulanganAsc(input, start+1);
}

static void rekrusifDesc()
{
    Console.WriteLine("==== Rekrusif Function ====");
    Console.WriteLine("Masukkan input : ");
    int input = int.Parse(Console.ReadLine());

    perulanganDesc(input);
}

static int perulanganDesc (int input)
{
    if (input == 1)
    {
        Console.WriteLine($"{input}");
        return input;
    }

    Console.WriteLine($"{input}");
    return perulanganDesc(input - 1);
}

static void returnTypePenjumlahan()
{
    Console.WriteLine("====== Method Return Type ======");
    Console.WriteLine("Masukkan Jumlah Mangga = ");
    int mangga = int.Parse(Console.ReadLine());

    Console.WriteLine("Masukkan Jumlah Apel = ");
    int apel = int.Parse(Console.ReadLine());

    int result = penjumlahan(mangga, apel);

    Console.WriteLine("Hasil Penjumlahan mangga dan apel adalah " + result);
}

static int penjumlahan(int mangga, int apel)
{
    return mangga+ apel;
}

static void overriding()
{
    Console.WriteLine("===== OVERRIDING =====");
    Kucing kucing = new Kucing();
    Paus paus = new Paus();

    kucing.pindah();
    paus.pindah();
}

static void inheritance()
{
    Console.WriteLine("==== Inheritance ====");

    TypeMobil typeMobil = new TypeMobil();
    typeMobil.Civic();
}

static void encasulaption()
{
    Console.WriteLine("===== Encasulaption =====");
    PersegiPanjang pp = new PersegiPanjang();

    pp.panjang = 10;
    pp.lebar = 20;
    pp.TampilkanData();
}

static void construcor()
{
    Console.WriteLine("===== Constructor =====");

    Mobil mobil = new Mobil("B1234ZTN");

    string platNo = mobil.getPlatNo();

    Console.WriteLine($"Mobil dengan Nomor Polisi : {platNo}");
}

static void objectClass()
{
    Console.WriteLine("==== Object Class ====");
    Mobil mobil = new Mobil("") { nama = "Ferari", kecepatan = 0, bensin=10, posisi=0};
    Mobil mobil2 = new Mobil("");
    mobil2.nama = "Lamborgini";
    mobil2.kecepatan = 0;
    mobil2.bensin = 10;
    mobil2.posisi = 0;

    mobil.percepat();
    mobil.maju();
    mobil.isiBensin(20.5);
    mobil.utama();
}

static void abstractClass()
{
    Console.WriteLine("===== Abstract Class =====");

    Console.Write("Masukkan input X : ");
    int x = int .Parse(Console.ReadLine());
    Console.Write("Masukkan input Y : ");
    int y = int .Parse(Console.ReadLine()); 

    TestTurunan cal = new TestTurunan();

    int jumlah = cal.Jumlah(x, y);
    int kurang = cal.Kurang(x, y);

    Console.WriteLine("Hasil X + Y = " + jumlah);
    Console.WriteLine("Hasil X - Y = " + kurang);
}