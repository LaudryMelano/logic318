﻿
//Break();
//Continue();
//stringToCharArray();
//indexOf();
//indexOf2();
//dateTime();
//dateTimeParsing();
//dateTimeProperties();
timeSpan();

static void Break()
{
    for(int i = 0; i < 10; i++)
    {
        if(i == 5)
        {
            break;
        }
        Console.WriteLine(i);
    }
}

static void Continue()
{
    for (int i = 0; i < 10; i++)
    {
        if (i == 7)
        {
            continue;
        }
        Console.WriteLine(i);
    }
}

static void stringToCharArray()
{
    Console.WriteLine("==== STRING TO CHAR ARRAY ====");
    Console.Write("Masukkan Kalimat : ");
    char[] charArr = Console.ReadLine().ToCharArray();

    for (int i = 0; i < charArr.Length; i++)
    {
        Console.WriteLine(charArr[i]);
    }
}

static void indexOf()
{
    List<int> list = new List<int> (){1,2,3,4,5,6};
    int[] numArr = new int[] {5,6,6,7,8,9,10};

    Console.Write("Masukkan item(value) : ");
    int item = int.Parse(Console.ReadLine());

    int countList = list.Where(a => a == 5).Count();
    int countArr = numArr.Where(a => a == 6).Count();

    Console.WriteLine();
    
    Console.WriteLine($"Banyaknya item {item} pada list " + countList);
    Console.WriteLine($"Banyaknya item {item} pada Array " + countArr);
    
    Console.WriteLine();


    int indexList = list.IndexOf(item);
    int indexArray = Array.IndexOf(numArr, item);

    if(indexList != -1)
    {
        Console.WriteLine("List Element {0} is found at index {1}", item, indexList);
    }
    else
    {
        Console.WriteLine("Element not found in the given List and Array");
    }

    if (indexArray != -1)
    {
        Console.WriteLine("Array Element {0} is found at index {1}", item, indexArray);
    }
    else
    {
        Console.WriteLine("Element not found in the given List and Array");
    }
}
static void indexOf2()
{
    List<int> list = new List<int> (){1,2,3,4,5,6};
    int[] numArr = new int[] {5,6,7,8,9,10};

    Console.Write("Masukkan item(value) : ");
    int[] item = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

    Console.WriteLine();

    int[] indexList = new int[item.Length];
    int[] indexArray = new int[item.Length];

    for (int i = 0; i < item.Length; i++)
    {
        indexList[i] = list.IndexOf(item[i]);
        indexArray[i] = Array.IndexOf(numArr, item[i]);

        if (indexList[i] != -1)
        {
            Console.WriteLine("List Element {0} is found at index {1}", item[i], indexList[i]);
        }
        else
        {
            Console.WriteLine("Element not found in the given List");
        }

        if (indexArray[i] != -1)
        {
            Console.WriteLine("Array Element {0} is found at index {1}", item[i], indexArray[i]);
        }
        else
        {
            Console.WriteLine("Element not found in the given Array");
        }
    }

}

static void dateTime()
{
    DateTime dt1 = new DateTime();
    Console.WriteLine(dt1);

    DateTime dt2 = DateTime.Now;
    Console.WriteLine(dt2);

    DateTime dt3 = new DateTime(2023, 5, 4);
    Console.WriteLine(dt3);

    DateTime dt4 = new DateTime(2023, 5, 4, 10, 15, 12);
    Console.WriteLine(dt4);
}

static void dateTimeParsing()
{
    Console.WriteLine("===== Date Time Pasring =====");

    Console.Write("Masukkan Date Time (dd/MM/yyyy) : ");
    string data = Console.ReadLine();

    try
    {
        DateTime date = DateTime.ParseExact(data, "dd/MM/yyyy", null);
        Console.WriteLine(date);
    }
    catch(Exception ex) 
    {
        Console.WriteLine("Masukkan format yang benar!");
        Console.WriteLine("Error : " +  ex.Message);
    }

}

static void dateTimeProperties()
{
    Console.WriteLine("===== DATE TIME PROPERTIES =====");

    DateTime date = new DateTime(2023, 5, 4, 11, 1, 45);

    int tahun = date.Year;//2023
    int bulan = date.Month;//5
    int hari = date.Day;//4
    int jam = date.Hour;
    int menit = date.Minute;
    int detik = date.Second;
    int weekDay = (int)date.DayOfWeek;
    string strHari = date.ToString("dddd");
    string strHari2 = date.DayOfWeek.ToString();

    Console.WriteLine();
    Console.WriteLine($"Tahun   : {tahun}");
    Console.WriteLine($"Bulan   : {bulan}");
    Console.WriteLine($"Hari    : {hari}");
    Console.WriteLine($"Jam     : {jam}");
    Console.WriteLine($"Menit   : {menit}");
    Console.WriteLine($"Detik   : {detik}");
    Console.WriteLine($"WeekDay : {weekDay}");
    Console.WriteLine($"Hari    : {strHari}");
    Console.WriteLine($"Hari    : {strHari2}");
}

static void timeSpan()
{
    DateTime date1 = new DateTime(2023, 5, 4, 11, 15, 45);
    DateTime date2 = new DateTime(2023, 7, 14, 11, 15, 45);

    //Calculate interval beetwen two dates
    //TimeSpan interval = date1.Subtract(date2);
    TimeSpan interval = date2 - date1;

    Console.WriteLine("Number of Days : {0}", interval.Days);
    Console.WriteLine("Total Days     : {0}", interval.TotalDays);
    Console.WriteLine("Number of Hours : {0}", interval.Hours);
    Console.WriteLine("Total Number of Hours : {0}", interval.TotalHours);
    Console.WriteLine("Number of Minutes : {0}", interval.Minutes);
    Console.WriteLine("Total Number of Minutes : {0}", interval.TotalMinutes);
    Console.WriteLine("Number of Seconds : {0}", interval.Seconds);
    Console.WriteLine("Total Number of Seconds : {0}", interval.TotalSeconds);
    Console.WriteLine("Number of Milliseconds : {0}", interval.Milliseconds);
    Console.WriteLine("Total Number of Millisconds : {0}", interval.TotalMilliseconds);
    Console.WriteLine("Ticks : {0}", interval.Ticks);
}