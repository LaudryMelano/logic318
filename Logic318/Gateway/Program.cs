﻿// See https://aka.ms/new-console-template for more information
//Console.WriteLine("Hello, World!");

namespace Gateaway;

public class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("===== Welcome to Batch 318 =====");
        Console.WriteLine();

        string answer = "t";

        while (answer.ToLower() == "t")
        {
            Console.Write("Pilih Hari : ");
            int hari = int.Parse(Console.ReadLine());

            switch (hari)
            {
                case 1:
                    Logic01.Program program = new Logic01.Program();
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    Logic05.Program program05 = new Logic05.Program();
                    break;
                case 6:
                    Logic06.Program program06 = new Logic06.Program();
                    break;
                case 7:
                    Logic07.Program program07 = new Logic07.Program();
                    break;
                case 8:
                    break;
                case 9:
                    Logic09.Program program09 = new Logic09.Program();
                    break;
                case 10:
                    Logic10.Program program10 = new Logic10.Program();
                    break;
                default:
                    Console.WriteLine("Tidak ada dalam menu");
                    break;
            }
            Console.WriteLine();
            Console.Write("Keluar Aplikasi? (Y/T) : ");
            answer = Console.ReadLine();
        }

        Console.WriteLine();
        Console.Write("Press Any Key...");
        Console.ReadKey();
    }
}
