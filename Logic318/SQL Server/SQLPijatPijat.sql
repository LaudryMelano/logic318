create database PijatTerserah
use DB_Pijat
drop database 

create table Grade(ID_Grade varchar(5) primary key,
type varchar(15) not null);

create table Provinsi(ID_Provinsi varchar(5) primary key,
Nama varchar(35) not null);

create table Kota(ID_Kota varchar(5) primary key,
Nama varchar(35) not null, ID_Provinsi varchar(5) foreign key references Provinsi(ID_Provinsi));

create table Member(ID_Member varchar(5) primary key,
Nama_Depan varchar(25) not null, Nama_Belakang varchar(35) null,
Alamat varchar(500) null, ID_Kota varchar(5) foreign key references Kota(ID_Kota),
ID_Grade varchar(5) foreign key references Grade(ID_Grade));

create table Job_Title(ID_Job varchar(5) primary key,
Title varchar(25) not null);

create table Employee(ID_Employee varchar(5) primary key,
Nama_Depan varchar(25) not null, Nama_Belakang varchar(35) null,
ID_Job varchar(5) foreign key references Job_Title(ID_Job));

create table Booking(ID_Booking varchar(4) primary key,
Tanggal DateTime not null,
ID_Member varchar(5) foreign key references Member(ID_Member));

--create table Type_Message(ID_Type_Message varchar(5) primary key,
--ID_Booking varchar(4) foreign key references Booking(ID_Booking),
--Tipe varchar(25) not null);

create table Massage(ID_Massage varchar(5) primary key,
Tipe varchar(25) not null, Harga money not null);

create table Goods(ID_Goods varchar(5) primary key,
Tipe varchar(25) not null,Quantity int not null, Harga money not null);

create table BookingMessage(ID_Booking_Message varchar(5) primary key,
ID_Booking varchar(4) foreign key references Booking(ID_Booking),
ID_Massage varchar(5) foreign key references Massage(ID_Massage),
Masanger varchar(5) foreign key references Employee(ID_Employee),
Harga money not null);

create table BookingGoods(ID_Booking_Goods varchar(5) primary key,
ID_Booking varchar(4) foreign key references Booking(ID_Booking),
ID_Goods varchar(5) foreign key references Goods(ID_Goods),
Quantity int not null, Harga money not null);

create table Invoice(ID_Invoice varchar(9) primary key,
Tanggal_Invoice datetime not null,
ID_Booking varchar(4) foreign key references Booking(ID_Booking),
Kasir varchar(5) foreign key references Employee(ID_Employee));

insert into Provinsi(ID_Provinsi,Nama)
values
--('PV001','Banten');
('PV002','Jawa Barat');
insert into Kota(ID_Kota, Nama, ID_Provinsi)
values
--('KT001','Tanggerang Selatan','PV001'),
('KT002','Depok','PV002'),
('KT003','Bogor','PV002')

insert into Grade(ID_Grade,[type])
values
--('GD001','Gold');
('GD002','Bronze'),
('GD003','Silver'),
('GD004','Platinum')

insert into Member(ID_Member, Nama_Depan, Nama_Belakang, Alamat, ID_Kota, ID_Grade)
values
('M0983','Arya','Sapta','Jl. Merpati','KT002','GD002'),
('M0984','Rico','Ceper','Jl. Kenangan','KT003','GD003'),
('M0985','Budi',null,'Jl. Flamboyan','KT001','GD004'),
('M0986','Raditya','Dika','Jl. Ampera','KT002','GD004')

insert into Job_Title(ID_Job, Title)
values('JB001','Massager'),('JB002','Kasir');

insert into Employee(ID_Employee, Nama_Depan, Nama_Belakang,ID_Job)
values
--('E0001','Wahyu',null,'JB001'),
--('E0002','Nino',null,'JB002'),
('E0003','Tono',null,'JB001'),
('E0004','Ayu',null,'JB001')


insert into Booking(ID_Booking, Tanggal, ID_Member)
values
--('B098','20230104','M0982')
--('B099','20230110','M0983'),
--('B100','20230206','M0984'),
--('B101','20230216','M0985'),
--('B102','20230309','M0986'),
--('B103','20230505','M0982')
('B104','20230512','M0983')

insert into Massage(ID_Massage, Tipe, Harga)
values
--('M0001','Full Body',120000);
('M0002','Half Body',80000),
('M0003','Kerik',50000)

insert into BookingMessage(ID_Booking_Message, ID_Booking, ID_Massage, Masanger, Harga)
values
--('BM001','B098','M0001','E0001',120000),
--('BM002','B099','M0001','E0001',120000),
--('BM003','B100','M0001','E0001',120000),
--('BM004','B101','M0002','E0003',80000),
--('BM005','B102','M0002','E0004',80000),
('BM006','B103','M0001','E0001',120000),
('BM007','B104','M0003','E0001',50000)

select * from BookingMessage

insert into Goods(ID_Goods, Tipe, Quantity, Harga)
values('G0001','Miyak Terapi',60,25000),('G0002','Wedang Jahe',100,15000),
('G0003','Handuk',120,46500);

update Goods set tipe = 'Minyak Terapi' where ID_Goods = 'G0001'

insert into BookingGoods(ID_Booking_Goods, ID_Booking, ID_Goods, Quantity, Harga)
values
--('BG001','B098','G0001',1,25000),
--('BG002','B098','G0002',1,15000),
--('BG003','B098','G0003',1,46500);
--('BG004','B100','G0001',1,25000),
--('BG005','B101','G0002',1,15000),
--('BG006','B101','G0003',1,46500);
('BG007','B100','G0001',2,25000)


update BookingGoods set harga = 15000 where ID_Booking_Goods = 'BG002'

insert into Invoice(ID_Invoice, Tanggal_Invoice, ID_Booking, Kasir)
values
--('M23010923','20230105','B098','E0002'),
--('M23011023','20230111','B099','E0002'),
--('M23012023','20230207','B100','E0002'),
--('M23013023','20230218','B101','E0002'),
--('M23014023','20230310','B102','E0002')
('M23015023','20230505','B103','E0002'),
('M23016023','20230512','B104','E0002')

alter view DataGathering
as
select iv.Tanggal_Invoice [Date],iv.Kasir, iv.ID_Invoice [Reff], bo.ID_Booking [Book No], bo.Tanggal [Tgl], 
mem.ID_Member [MemberId],mem.Nama_Depan [NamaCustomer],mem.Alamat [Alamat], kt.Nama [Kota], prov.Nama [Provinsi],gd.ID_Grade[GradeId],gd.[type] [Grade],
'Goods' as [type], god.ID_Goods [LayananID],god.Tipe [detailType], '' as [Masanger],bogo.Quantity [Quantity], bogo.Harga [Price] 
from Invoice iv
join Booking bo on iv.ID_Booking=bo.ID_Booking
join BookingGoods bogo on bo.ID_Booking=bogo.ID_Booking
join Goods god on god.ID_Goods=bogo.ID_Goods
join Member mem on mem.ID_Member=bo.ID_Member
join Grade gd on gd.ID_Grade=mem.ID_Grade
join Kota kt on kt.ID_Kota=mem.ID_Kota
join Provinsi prov on prov.ID_Provinsi=kt.ID_Provinsi
union
select iv.Tanggal_Invoice [Date],iv.Kasir, iv.ID_Invoice [Reff], bo.ID_Booking [Book No], bo.Tanggal [Tgl], 
mem.ID_Member [MemberId],mem.Nama_Depan [NamaCustomer],mem.Alamat [Alamat], kt.Nama [Kota], prov.Nama [Provinsi], gd.ID_Grade[GradeId], gd.[type] [Grade], 
'Message' as [type], mas.ID_Massage [LayananID], mas.Tipe [detailType], em.Nama_Depan [Masanger],0 as [Quantity], boma.Harga [Price] 
from Invoice iv
join Booking bo on iv.ID_Booking=bo.ID_Booking
join BookingMessage boma on bo.ID_Booking=boma.ID_Booking 
join Massage mas on mas.ID_Massage=boma.ID_Massage
join Employee em on boma.Masanger=em.ID_Employee
join Job_Title joti on joti.ID_Job=em.ID_Job
join Member mem on mem.ID_Member=bo.ID_Member
join Grade gd on gd.ID_Grade=mem.ID_Grade
join Kota kt on kt.ID_Kota=mem.ID_Kota
join Provinsi prov on prov.ID_Provinsi=kt.ID_Provinsi

-- 00 Tampilkan rekapitulasi
select [Date],Reff, [Book No], Tgl, MemberId,NamaCustomer,
Alamat, Kota, Provinsi,Grade,[type], detailType, Masanger,
em.Nama_Depan as Kasir,Price,Quantity,
CASE
	WHEN [type] = 'Message' THEN CAST((dg.Price*0.05) as decimal (18,2))
	ELSE 0
END [Service],
CASE
	WHEN [type] = 'Goods' THEN CAST(((Price*Quantity)*0.11) as decimal (18,2))
	ELSE CAST((Price*0.11) as decimal (18,2))
END [Tax],
CASE
	WHEN [type] = 'Message' THEN CAST((Price*0.11)+(dg.Price*0.05)+(Price) as decimal (18,2))
	ELSE CAST(((Price*Quantity)*0.11)+(Price*Quantity) as decimal (18,2))
END [Payment]
from DataGathering dg
join Employee em on em.ID_Employee=dg.Kasir

-- 01 Tampilkan Masseus dengan pelanggan terbanyak
select CONCAT(emp.Nama_Depan, '', emp.Nama_Belakang) Masseus, count(bm.Masanger) Pelanggan
	from Employee emp
	join BookingMessage bm on emp.ID_Employee = bm.Masanger
	group by bm.Masanger, emp.Nama_Depan, emp.Nama_Belakang
having count(bm.Masanger) = (
select max(Total)
from(
	select count(Masanger) Total
	from BookingMessage
	group by Masanger
)msgr)

--02 Tampilkan Produk goods yang paling laris

select gds.Tipe Produk, sum(bg.Quantity) Quantity
from Goods gds
join BookingGoods bg on gds.ID_Goods = bg.ID_Goods
group by bg.ID_Goods, gds.Tipe
having sum(bg.Quantity) = (select max(Qty)
from(
	select sum(Quantity) Qty
	from BookingGoods
	group by ID_Goods
	)maxGds)

--03 Tampilkan service massage yang paling sedikit peminat
select ms.Tipe,count(bm.ID_Massage) Pelanggan
from BookingMessage bm
join Massage ms on bm.ID_Massage = ms.ID_Massage
group by bm.ID_Massage, ms.Tipe
having count(bm.ID_Massage) = (select min(massage)
from(
	select count(ID_Massage) massage
	from BookingMessage 
	group by ID_Massage
)minMsg)

--04 Tampilkan jumlah service setiap bulannya.
select DATENAME(mm,bk.Tanggal) Bulan, ms.Tipe,count(bm.ID_Massage) Pelanggan
from BookingMessage bm
join Massage ms on bm.ID_Massage = ms.ID_Massage
join Booking bk on bm.ID_Booking = bk.ID_Booking
group by bm.ID_Massage, ms.Tipe, MONTH(bk.Tanggal), DATENAME(mm,bk.Tanggal)

select DATENAME(mm, iv.Tanggal_Invoice) Bulan, SUM(CAST((boma.Harga*0.05)as decimal(18,2))) [Service]
from Invoice iv
join Booking bo on iv.ID_Booking=bo.ID_Booking
join BookingMessage boma on bo.ID_Booking=boma.ID_Booking 
join Massage mas on mas.ID_Massage=boma.ID_Massage
join Employee em on boma.Masanger=em.ID_Employee
join Job_Title joti on joti.ID_Job=em.ID_Job
join Member mem on mem.ID_Member=bo.ID_Member
join Grade gd on gd.ID_Grade=mem.ID_Grade
join Kota kt on kt.ID_Kota=mem.ID_Kota
join Provinsi prov on prov.ID_Provinsi=kt.ID_Provinsi
group by MONTH(iv.Tanggal_Invoice), DATENAME(mm, iv.Tanggal_Invoice)
order by MONTH(iv.Tanggal_Invoice)

select DATENAME(mm, iv.Tanggal_Invoice) Bulan,YEAR(iv.Tanggal_Invoice) Tahun, COUNT(boma.ID_Massage) [JumlahService] 
from Invoice iv
join Booking bo on iv.ID_Booking=bo.ID_Booking
join BookingMessage boma on bo.ID_Booking=boma.ID_Booking 
join Massage mas on mas.ID_Massage=boma.ID_Massage
join Employee em on boma.Masanger=em.ID_Employee
join Job_Title joti on joti.ID_Job=em.ID_Job
join Member mem on mem.ID_Member=bo.ID_Member
join Grade gd on gd.ID_Grade=mem.ID_Grade
join Kota kt on kt.ID_Kota=mem.ID_Kota
join Provinsi prov on prov.ID_Provinsi=kt.ID_Provinsi
group by MONTH(iv.Tanggal_Invoice), DATENAME(mm, iv.Tanggal_Invoice), YEAR(iv.Tanggal_Invoice)
order by MONTH(iv.Tanggal_Invoice), YEAR(iv.Tanggal_Invoice)

--05 Tampilkan tahun bulan penjualan terbanyak
select Tahun, Bulan,SUM(Payment) TotalPenjualan 
from
(select DATENAME(mm,[Date]) Bulan, MONTH([Date]) BulangAngka, YEAR([Date]) Tahun,
CASE
	WHEN [type] = 'Message' THEN SUM(CAST((Price*0.11)+(Price*0.05)+(Price) as decimal (18,2)))
	ELSE SUM(CAST(((Price*Quantity)*0.11)+(Price*Quantity) as decimal (18,2)))
END [Payment]
from DataGathering 
GROUP BY DATENAME(mm,[Date]),MONTH([Date]), YEAR([Date]), [type]
)sub
GROUP BY Bulan,BulangAngka, Tahun
HAVING SUM(Payment) = (SELECT MAX(sub1.TotalPendapatan) 
FROM(
select Tahun, Bulan,SUM(Payment) TotalPendapatan
from
(select DATENAME(mm,[Date]) Bulan, MONTH([Date]) BulangAngka, YEAR([Date]) Tahun,
CASE
	WHEN [type] = 'Message' THEN SUM(CAST((Price*0.11)+(Price*0.05)+(Price) as decimal (18,2)))
	ELSE SUM(CAST(((Price*Quantity)*0.11)+(Price*Quantity) as decimal (18,2)))
END [Payment]
from DataGathering 
GROUP BY DATENAME(mm,[Date]),MONTH([Date]), YEAR([Date]), [type]
)sub
GROUP BY Bulan,BulangAngka, Tahun
)sub1
)

--06 Tampilkan masing-masing total pendapatan service/massage & product/goods.
select [type] [Type], 
CASE
	WHEN [type] = 'Message' THEN SUM(CAST((Price*0.11)+(Price*0.05)+(Price) as decimal (18,2)))
	ELSE SUM(CAST(((Price*Quantity)*0.11)+(Price*Quantity) as decimal (18,2)))
END [Payment]
from DataGathering 
GROUP BY [type]

--periodik
select [type] [Type], MONTH([Date]), YEAR([Date]),
CASE
	WHEN [type] = 'Message' THEN SUM(CAST((Price*0.11)+(Price*0.05)+(Price) as decimal (18,2)))
	ELSE SUM(CAST(((Price*Quantity)*0.11)+(Price*Quantity) as decimal (18,2)))
END [Payment]
from DataGathering 
GROUP BY [type], MONTH([Date]), YEAR([Date])

--07 Dalam meningkatkan minat pelanggan, diadakan promo untuk para member dan mendapat potongan.
--		Bronze 2.5% untuk service tertentu
--		Silver 5% untuk service tertentu
--		Gold 5% untuk service & product tertentu
--		Platinum 8% untuk service & product tertentu
--	a. Tambahkan tabel dan/atau kolom diskon per grade dan product.
--	b. Tampilkan transaksi dengan diskon untuk customer sesuai grade-nya.

-- a
create table Diskon(
ID int identity(1,1),
ID_Grade varchar(5) foreign key references Grade(ID_Grade),
ID_Layanan varchar(5));

INSERT INTO DISKON(ID_Grade, ID_Layanan)
VALUES
('GD002', 'M0002'),
('GD003', 'M0001'),
('GD001', 'M0001'),
('GD001', 'G0002'),
('GD004', 'M0001'),
('GD004', 'G0003')



ALTER TABLE Diskon ADD [Diskon(%)] decimal
ALTER TABLE Diskon ALTER COLUMN [Diskon(%)] decimal(3,3)
UPDATE Diskon SET [Diskon(%)] = 0.025 WHERE ID_Grade = 'GD002'
UPDATE Diskon SET [Diskon(%)] = 0.05 WHERE ID_Grade = 'GD003'
UPDATE Diskon SET [Diskon(%)] = 0.05 WHERE ID_Grade = 'GD001'
UPDATE Diskon SET [Diskon(%)] = 0.08 WHERE ID_Grade = 'GD004' 
UPDATE Diskon SET ID_Layanan = 'M0001' WHERE ID=3
select * from Diskon
select * from Goods
select * from Massage

--b. 
select [Date],Reff, [Book No], Tgl, MemberId,NamaCustomer,
Alamat, Kota, Provinsi,Grade,[type], detailType, Masanger,
em.Nama_Depan as Kasir,Price,Quantity,
CASE
	WHEN [type] = 'Message' THEN CAST((dg.Price*0.05) as decimal (18,2))
	ELSE 0
END [Service],
CASE
	WHEN [type] = 'Goods' THEN CAST(((Price*Quantity)*0.11) as decimal (18,2))
	ELSE CAST((Price*0.11) as decimal (18,2))
END [Tax],
CASE
	WHEN dg.gradeid = 'GD002' AND dg.LayananID = 'M0002' THEN  CAST((Price*0.025) as decimal (18,2))
	WHEN dg.gradeid = 'GD003' AND dg.LayananID = 'M0001' THEN  CAST((Price*0.05) as decimal (18,2))
	WHEN dg.gradeid = 'GD001' AND (dg.LayananID = 'M0001' OR dg.LayananID = 'G0002') THEN  CAST((Price*0.05) as decimal (18,2))
	WHEN dg.gradeid = 'GD004' AND (dg.LayananID = 'M0001' OR dg.LayananID = 'G0003') THEN  CAST((Price*0.08) as decimal (18,2))
	ELSE 0
END [Diskon],
CASE
	WHEN [type] = 'Message' AND (dg.gradeid = 'GD001' AND dg.LayananID = 'M0001') THEN CAST((Price*0.11)+(Price*0.05)+(Price)-(Price*0.05) as decimal (18,2))
	WHEN [type] = 'Message' AND (dg.gradeid = 'GD002' AND dg.LayananID = 'M0002') THEN CAST((Price*0.11)+(Price*0.05)+(Price)-(Price*0.025) as decimal (18,2))
	WHEN [type] = 'Message' AND (dg.gradeid = 'GD003' AND dg.LayananID = 'M0001') THEN CAST((Price*0.11)+(Price*0.05)+(Price)-(Price*0.05) as decimal (18,2))
	WHEN [type] = 'Message' AND (dg.gradeid = 'GD004' AND dg.LayananID = 'M0001') THEN CAST((Price*0.11)+(Price*0.05)+(Price)-(Price*0.08) as decimal (18,2))
	WHEN [type] = 'Message' THEN CAST((Price*0.11)+(Price*0.05)+(Price) as decimal (18,2))
	WHEN [type] = 'Goods' AND (dg.gradeid = 'GD001' AND dg.LayananID = 'G0002') THEN CAST(((Price*Quantity)*0.11)+(Price*Quantity)-(Price*0.05) as decimal (18,2))
	WHEN [type] = 'Goods' AND (dg.gradeid = 'GD004' AND dg.LayananID = 'G0003') THEN CAST(((Price*Quantity)*0.11)+(Price*Quantity)-(Price*0.08) as decimal (18,2))
	WHEN [type] = 'Goods' THEN CAST(((Price*Quantity)*0.11)+(Price*Quantity) as decimal (18,2))
END [Payment]
from DataGathering dg
join Employee em on em.ID_Employee=dg.Kasir

------------
select [Date],Reff, [Book No], Tgl, MemberId,NamaCustomer,
Alamat, Kota, Provinsi,Grade,[type], detailType, Masanger,
em.Nama_Depan as Kasir,Price,Quantity,
CASE
	WHEN [type] = 'Message' THEN CAST((dg.Price*0.05) as decimal (18,2))
	ELSE 0
END [Service],
CASE
	WHEN [type] = 'Goods' THEN CAST(((Price*Quantity)*0.11) as decimal (18,2))
	ELSE CAST((Price*0.11) as decimal (18,2))
END [Tax],
dis.[Diskon(%)] DiskonPersen,
CASE
	WHEN [type] = 'Message' AND (dg.gradeid = 'GD001' AND dg.LayananID = 'M0001') THEN CAST((Price*0.11)+(Price*0.05)+(Price)-(Price*0.05) as decimal (18,2))
	WHEN [type] = 'Message' AND (dg.gradeid = 'GD002' AND dg.LayananID = 'M0002') THEN CAST((Price*0.11)+(Price*0.05)+(Price)-(Price*0.025) as decimal (18,2))
	WHEN [type] = 'Message' AND (dg.gradeid = 'GD003' AND dg.LayananID = 'M0001') THEN CAST((Price*0.11)+(Price*0.05)+(Price)-(Price*0.05) as decimal (18,2))
	WHEN [type] = 'Message' AND (dg.gradeid = 'GD004' AND dg.LayananID = 'M0001') THEN CAST((Price*0.11)+(Price*0.05)+(Price)-(Price*0.08) as decimal (18,2))
	WHEN [type] = 'Message' THEN CAST((Price*0.11)+(Price*0.05)+(Price) as decimal (18,2))
	WHEN [type] = 'Goods' AND (dg.gradeid = 'GD001' AND dg.LayananID = 'G0002') THEN CAST(((Price*Quantity)*0.11)+(Price*Quantity)-(Price*0.05) as decimal (18,2))
	WHEN [type] = 'Goods' AND (dg.gradeid = 'GD004' AND dg.LayananID = 'G0003') THEN CAST(((Price*Quantity)*0.11)+(Price*Quantity)-(Price*0.08) as decimal (18,2))
	WHEN [type] = 'Goods' THEN CAST(((Price*Quantity)*0.11)+(Price*Quantity) as decimal (18,2))
END [Payment]
from DataGathering dg
join Employee em on em.ID_Employee=dg.Kasir
left join Diskon dis on dg.GradeId = dis.ID_Grade AND dg.LayananID = dis.ID_Layanan

select * from Diskon
-------------

select dg.LayananID,dg.GradeId, dis.ID_Grade, dis.ID_Layanan,
dis.[Diskon(%)]
from DataGathering dg
join Employee em on em.ID_Employee=dg.Kasir
left join Diskon dis on dg.GradeId = dis.ID_Grade AND dg.LayananID = dis.ID_Layanan

select * from Diskon


--08 Tambahkan jenis kelamin untuk member/pelanggan & untuk Masseus, Jenis kelamin char(1) L/P
ALTER TABLE Member
ADD Jenis_Kelamin Char(1) null check(Jenis_Kelamin = 'L' or Jenis_Kelamin ='P')

ALTER TABLE Employee
ADD Jenis_Kelamin Char(1) null check(Jenis_Kelamin = 'L' or Jenis_Kelamin ='P')

select * from member

--09 Tampilkan jumlah customer per Masseus dan bonus dimana Jumlah customer 5-10 mendapat 5% dari harga service, 
--sedangkan >10 mendapat 8% dari harga service.
SELECT CONCAT(emp.Nama_Depan, '', emp.Nama_Belakang) Masseus, count(bm.Masanger) Customer,
CASE
	WHEN count(bm.Masanger)>=5 AND count(bm.Masanger) <=10 THEN SUM(CAST((bm.Harga*0.05)*0.05 as decimal (18,2)))
	WHEN count(bm.Masanger)>10  THEN SUM(CAST((bm.Harga*0.05)*0.08 as decimal (18,2)))
	ELSE 0
END Bonus
FROM Employee emp
JOIN BookingMessage bm 
	ON emp.ID_Employee = bm.Masanger
GROUP BY bm.Masanger, emp.Nama_Depan, emp.Nama_Belakang

--10 Tampilkan tren jumlah customer harian dengan nama hari (Sunday-Saturday) segala waktu.
SELECT hr.Nama Hari, COUNT(bk.ID_Member) Customer
FROM Invoice inv
JOIN Booking bk ON inv.ID_Booking = bk.ID_Booking
JOIN Member mb ON bk.ID_Member = mb.ID_Member
FULL JOIN Hari hr ON hr.Nama = DATENAME(WEEKDAY, inv.Tanggal_Invoice)
GROUP BY hr.Nama , hr.id
ORDER BY hr.id

CREATE TABLE Hari(
ID int primary key identity(1,1),
Nama varchar(20)
)

INSERT INTO Hari(Nama)
VALUES
('Monday'), ('Tuesday'), ('Wednesday'), ('Thursday'), ('Friday'),
('Saturday'), ('Sunday')





