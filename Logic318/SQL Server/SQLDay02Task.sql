CREATE DATABASE db_penerbit

CREATE TABLE tblPengarang ( 
 id int primary key identity (1,1),
 kd_Pengarang varchar(7) not null,
 nama varchar(30) not null,
 alamat varchar(80) not null,
 kota varchar(15) not null,
 kelamin varchar(1) not null
)

CREATE TABLE tblGaji (
 id int primary key identity (1,1),
 kd_pengarang varchar(7) not null,
 nama varchar(30) not null,
 gaji decimal(18,4) not null 
)

INSERT INTO tblPengarang(kd_Pengarang,nama,alamat,kota,kelamin)
values
('P0001', 'Ashadi', 'Jl. Beo 25', 'Yogya', 'P'),
('P0002', 'Rian', 'Jl. Solo 123', 'Yogya', 'P'),
('P0003', 'Suwadi', 'Jl. Semangka 13', 'Bandung', 'P'),
('P0004', 'Siti', 'Jl. Durian 15', 'Solo', 'W'),
('P0005', 'Amir', 'Jl. Gajah 33', 'Kudus', 'P'),
('P0006', 'Suparman', 'Jl. Harimau 25', 'Jakarta', 'P'),
('P0007', 'Jaja', 'Jl. Singa 7', 'Bandung', 'P'),
('P0008', 'Saman', 'Jl. Naga 12', 'Yogya', 'P'),
('P0009', 'Anwar', 'Jl. Tidar 6A', 'Magelang', 'P'),
('P0010', 'Fatmawati', 'Jl. Renjana 4', 'Bogor', 'W')

UPDATE tblPengarang SET kota = 'Bogor' WHERE kd_Pengarang = 'P0010'

INSERT INTO tblGaji(kd_pengarang, nama, gaji)
VALUES
('P0002', 'Rian', 600000),
('P0005', 'Amir', 700000),
('P0004', 'Siti', 500000),
('P0003', 'Suwadi', 1000000),
('P0010', 'Fatmawati', 600000),
('P0008', 'Saman', 750000)


SELECT * FROM tblGaji order by gaji
SELECT * FROM tblPengarang

--- NO. 2 ---
CREATE VIEW vwPengarang
AS 
SELECT kd_Pengarang, nama, kota
FROM tblPengarang 

--- NO. 3 ---
--- A. Tampilkan �Kd_Pengarang, nama� yang dikelompokan atas nama --
SELECT kd_pengarang, nama 
FROM tblPengarang
ORDER BY nama 

--- B. Tampilkan �kota,Kd_Pengarang ,nama� yang dikelompokan atas kota. 
SELECT kota,kd_Pengarang, nama
FROM tblPengarang
ORDER BY kota

--- C. Hitung dan tampilkan jumlah pengarang ---
SELECT COUNT(kd_Pengarang) JumlahPengarang FROM tblPengarang 

--- D. Tampilkan record kota dan jumlah kotanya---
SELECT kota, count(kota) JumlahKota FROM tblPengarang
GROUP BY kota

--- E. Tampilkan record kota diatas 1 kota ---
SELECT kota, COUNT(kota) jmlKota FROM tblPengarang
GROUP BY kota
HAVING COUNT(kota) > 1

--- F. Tampilkan Kd_Pengarang yang terbesar dan terkecil ---
SELECT MAX(kd_Pengarang) KDTerbesar, MIN(kd_Pengarang) KDTerkecil FROM tblPengarang


--- NO. 5 ---

--- A. Tampilkan gaji tertinggi dan terendah ---
SELECT max(gaji) GajiTertinggi, min(gaji) GajiTerendah
FROM tblGaji

--- B. Tampilkan gaji diatas 600.000. ---
SELECT gaji 
FROM tblGaji 
WHERE gaji > 600000

--- C. Tampilkan jumlah gaji ---
SELECT sum(gaji) TotalGajiPengarang 
FROM tblGaji

--- D. Tampilkan jumlah gaji berdasarkan Kota ---
SELECT pg.kota, sum(gj.gaji) JumlahGaji 
FROM tblPengarang pg
JOIN tblGaji gj ON pg.kd_Pengarang = gj.kd_pengarang
GROUP BY pg.kota

--- E. Tampilkan seluruh record pengarang antara P0001-P0006 dari tabel pengarang ---
SELECT * FROM tblPengarang 
WHERE kd_Pengarang BETWEEN 'P0001' AND 'P0006'

SELECT * FROM tblPengarang 
WHERE kd_Pengarang >= 'P0001' AND kd_Pengarang <='P0006'

--- F. Tampilkan seluruh data yogya, solo, dan magelang dari tabel pengarang. ---
SELECT * FROM tblPengarang 
WHERE kota = 'solo' OR kota = 'magelang' OR kota = 'yogya'

SELECT * FROM tblPengarang
WHERE kota IN ('solo', 'magelang', 'yogya')

--- G. Tampilkan seluruh data yang bukan yogya dari tabel pengarang. ---
select * from tblPengarang where not kota = 'yogya'

--- H. Tampilkan seluruh data pengarang yang nama (dapat digabungkan atau terpisah):
-- a. dimulai dengan huruf [A] 
SELECT * FROM tblPengarang WHERE nama LIKE 'a%'

-- b. berakhiran [i]
SELECT * FROM tblPengarang WHERE nama LIKE '%i'

-- c. huruf ketiganya [a]
SELECT * FROM tblPengarang where nama LIKE '__a%'

-- d. tidak berakhiran [n]
select * from tblPengarang where not nama LIKE '%n'

--- I. Tampilkan seluruh data table tblPengarang dan tblGaji dengan Kd_Pengarang yang sama ---
SELECT * FROM tblPengarang pg 
JOIN tblGaji gj ON pg.kd_Pengarang = gj.kd_pengarang

--- J. Tampilan kota yang memiliki gaji dibawah 1.000.000 ---
SELECT pg.kota FROM tblPengarang pg 
JOIN tblGaji gj ON pg.kd_Pengarang = gj.kd_pengarang
WHERE gaji < 1000000

--- K. Ubah panjang dari tipe kelamin menjadi 10 ---
ALTER TABLE tblPengarang ALTER COLUMN kelamin VARCHAR(10)

--- L. Tambahkan kolom [Gelar] dengan tipe Varchar (12) pada tabel tblPengarang --
ALTER TABLE tblPengarang ADD gelar VARCHAR(12)

--- M. Ubah alamat dan kota dari Rian di table tblPengarang menjadi, Jl. Cendrawasih 65 dan Pekanbaru
UPDATE tblPengarang SET kota = 'Pekan Baru', alamat = 'Jl. Cendrawasih 65' where nama = 'rian'

---  ---
SELECT kelamin, count(kelamin) JumlahPengarang from tblPengarang 
group by kelamin

select * from vwPengarang