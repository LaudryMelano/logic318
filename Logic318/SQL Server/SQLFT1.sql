SELECT CONCAT(bio.first_name,' ', bio.last_name) [Fullname], 
CASE WHEN fa.status='anak' OR fa. status  = 'istri' OR fa. status = 'suami' THEN 'Sudah Menikah' END AS [Status Pernikahan]
FROM biodata bio
JOIN family fa ON fa.biodata_id = bio.id
where fa.status = 'anak'
GROUP BY bio.first_name, bio.last_name,fa. status

select *
from biodata bio
join employee emp ON bio.id = emp.biodata_id
join employee_position ep ON emp.id = ep.employee_id
join position p ON ep.position_id = p.id 


select emp.nip,CONCAT( bio.first_name, ' ',bio.last_name) FullName, SUM(datediff(day, lr.start_date,lr.end_date)) CUtiDiambil
from biodata bio
left join employee emp ON bio.id = emp.biodata_id
left join leave_request lr ON emp.id = lr.employee_id
group by emp.nip, bio.first_name,bio.last_name

select CONCAT( bio.first_name, ' ',bio.last_name) FullName, p.name, DATEDIFF(YEAR, bio.dob, GETDATE()) Umur,
CASE
	WHEN f.status = 'Anak' THEN count(f.status)
	ELSE 0
END JmlAnak
from biodata bio
left join employee emp ON bio.id = emp.biodata_id
join employee_position ep ON emp.id = ep.employee_id
join position p ON ep.position_id = p.id 
left join family f ON bio.id = f.biodata_id
GROUP BY bio.first_name, bio.last_name, bio.dob, p.name, f.status

select 
CASE
	WHEN emp.status = 'permanen' THEN count(emp.id)
	WHEN emp.status = 'kontrak' THEN count(emp.id)
END StatusKaryawan, emp.status
from biodata bio
join employee emp ON bio.id = emp.biodata_id
group by emp.status


select CONCAT( bio.first_name, ' ',bio.last_name) FullName, emp.nip, emp.status, emp.salary
from biodata bio
join employee emp ON bio.id = emp.biodata_id
WHERE bio.pob LIKE '%Jakarta%' AND bio.address NOT LIKE '%Jakarta%'