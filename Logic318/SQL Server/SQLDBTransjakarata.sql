CREATE DATABASE db_Transjakarta

CREATE TABLE Halte (
Id INT IDENTITY(1,1) NOT NULL,
Kode VARCHAR(20) PRIMARY KEY NOT NULL,
Nama VARCHAR(50) NOT NULL
)

CREATE TABLE Tarif(
Id INT IDENTITY(1,1) NOT NULL,
Kode VARCHAR(1) PRIMARY KEY NOT NULL,
Harga DECIMAL(18,4) NOT NULL
)

CREATE TABLE Bis(
Id INT IDENTITY(1,1) NOT NULL,
Kode VARCHAR(20) PRIMARY KEY NOT NULL,
Nama VARCHAR(50) NOT NULL
)

CREATE TABLE Jalur(
Id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
KodeBis VARCHAR(20) FOREIGN KEY REFERENCES Bis(Kode) NOT NULL,
KodeHalte VARCHAR(20) FOREIGN KEY REFERENCES Halte(Kode) NOT NULL,
)

CREATE TABLE Transaksi(
Id INT IDENTITY(1,1) NOT NULL,
KodeTransaksi VARCHAR(50) PRIMARY KEY NOT NULL,
KartuPengguna VARCHAR(20) NOT NULL,
JamTapIn DATETIME NOT NULL,
JamTapOut DATETIME NULL,
HalteAwal VARCHAR(20) NOT NULL,
CONSTRAINT FK_Transaksi_HalteAwal
FOREIGN KEY (HalteAwal) REFERENCES Halte(Kode),
HalteAkhir VARCHAR(20) NULL,
CONSTRAINT FK_Transaksi_HalteAkhir
FOREIGN KEY (HalteAkhir) REFERENCES Halte(Kode),
KodeTarif VARCHAR(1) FOREIGN KEY REFERENCES Tarif(Kode) NOT NULL,
)

INSERT INTO Halte
VALUES
('H0001','Ciledug'),
('H0002','Puri Beta 2'),
('H0003','Puri Beta 1'),
('H0004','Adam Malik'),
('H0005','JORR'),
('H0006','Swadarma'),
('H0007','Cipulir'),
('H0008','Seskoal'),
('H0009','Kebayoran Lama'),
('H0010','Velbak'),
('H0011','Mayestik'),
('H0012','CSW'),
('H0013','Tirtayasa'),
('H0014','Rawa Barat'),
('H0015','Tendean'),
('H0016','Pancoran Barat'),
('H0017','Tegal Parang'),
('H0018','Gatot Subroto Jamsostek'),
('H0019','Gatot Subroto LIPI'),
('H0020','Semanggi'),
('H0021','Karet Sudirman'),
('H0022','Dukuh Atas'),
('H0023','Patra Kuningan'),
('H0024','Departemen Kesehatan'),
('H0025','Gor Soemantri'),
('H0026','Karet Kuningan'),
('H0027','Kuningan Madya'),
('H0028','Setiabudi Aini Utara'),
('H0029','Latuharhari')

INSERT INTO Bis
VALUES
('13', 'CILEDUG - TENDEAN'),
('13B', 'PURI BETA - PANCORAN BARAT'),
('13C', 'PURI BETA 2 - DUKUH ATAS'),
('L13E', 'PURI BETA 2 - KUNINGAN')

INSERT INTO Tarif
VALUES
('A',3500),
('B',2000),
('C',0)

INSERT INTO Jalur
VALUES
('13','H0001'),
('13','H0002'),
('13','H0003'),
('13','H0004'),
('13','H0005'),
('13','H0006'),
('13','H0007'),
('13','H0008'),
('13','H0009'),
('13','H0010'),
('13','H0011'),
('13','H0012'),
('13','H0013'),
('13','H0014'),
('13','H0015'),
('13B','H0002'),
('13B','H0003'),
('13B','H0004'),
('13B','H0005'),
('13B','H0006'),
('13B','H0007'),
('13B','H0008'),
('13B','H0009'),
('13B','H0010'),
('13B','H0011'),
('13B','H0012'),
('13B','H0013'),
('13B','H0014'),
('13B','H0015'),
('13B','H0016'),
('13C','H0002'),
('13C','H0003'),
('13C','H0004'),
('13C','H0005'),
('13C','H0006'),
('13C','H0007'),
('13C','H0008'),
('13C','H0009'),
('13C','H0010'),
('13C','H0011'),
('13C','H0012'),
('13C','H0013'),
('13C','H0014'),
('13C','H0017'),
('13C','H0018'),
('13C','H0019'),
('13C','H0020'),
('13C','H0021'),
('13C','H0022'),
('13C','H0015'),
('13C','H0016'),
('L13E','H0002'),
('L13E','H0003'),
('L13E','H0004'),
('L13E','H0010'),
('L13E','H0012'),
('L13E','H0015'),
('L13E','H0023'),
('L13E','H0024'),
('L13E','H0025'),
('L13E','H0026'),
('L13E','H0027'),
('L13E','H0028'),
('L13E','H0029')

INSERT INTO Transaksi
VALUES
('TRAN00001','US00001','2023/04/25 08:32:00', '2023/04/25 08:57:00','H0014','H0016','A'),
('TRAN00002','US00003','2023/04/25 10:25:00', '2023/04/25 12:57:00','H0012','H0024','A'),
('TRAN00003','US00002','2023/04/26 06:20:00', '2023/04/26 06:55:00','H0023','H0029','B'),
('TRAN00004','US00002','2023/04/26 17:37:00', '2023/04/26 17:40:00','H0029','H0029','C'),
('TRAN00005','US00001','2023/04/26 07:28:00', '2023/04/26 08:10:00','H0010','H0015','A'),
('TRAN00006','US00004','2023/04/27 06:07:00', '2023/04/27 07:30:00','H0004','H0028','A'),
('TRAN00007','US00005','2023/04/27 09:05:00', '2023/04/27 09:07:00','H0027','H0027','C'),
('TRAN00008','US00006','2023/04/28 09:15:00', '2023/04/28 10:00:00','H0027','H0002','A'),
('TRAN00009','US00003','2023/04/29 05:32:00', '2023/04/29 06:55:00','H0028','H0003','B'),
('TRAN00010','US00002','2023/04/30 11:30:00', '2023/04/30 11:32:00','H0008','H0008','C'),
('TRAN00011','US00005','2023/04/30 19:43:00', '2023/04/30 20:18:00','H0022','H0013','A'),
('TRAN00012','US00001','2023/05/01 17:48:00', '2023/05/01 17:59:00','H0014','H0016','A'),
('TRAN00013','US00005','2023/05/02 05:36:00', '2023/05/02 05:45:00','H0015','H0016','B'),
('TRAN00014','US00003','2023/05/02 08:25:00', '2023/05/02 08:29:00','H0005','H0005','C'),
('TRAN00015','US00001','2023/05/02 14:23:00', '2023/05/02 15:37:00','H0007','H0026','A')

CREATE VIEW vwJamPeak
AS
SELECT
	jam.JamMasuk,
	COUNT(jam.JamMasuk) BanyakMasuk,
	jam.JamKeluar,
	COUNT(jam.JamMasuk) BanyakKeluar
FROM
(
	SELECT
		trs.KodeTransaksi,
		DATEPART(HOUR, trs.JamTapIn) JamMasuk,
		DATEPART(HOUR, trs.JamTapOut) JamKeluar
	FROM dbo.Transaksi trs
) jam
GROUP BY
	jam.JamMasuk,
	jam.JamKeluar

CREATE VIEW vwPeak
AS
	WITH ctePeak (Jam, BanyakOrang)
	AS
	(
		SELECT
			vJP.JamMasuk,
			COUNT(vJP.JamMasuk)
		FROM vwJamPeak vJP
		GROUP BY
			vJP.JamMasuk
		UNION
		SELECT
			vJP.JamKeluar,
			COUNT(vJP.JamKeluar)
		FROM vwJamPeak vJP
		WHERE
			vJP.JamKeluar != vJP.JamMasuk
		GROUP BY
			vJP.JamKeluar
	)
	SELECT
		cteP.Jam,
		SUM(cteP.BanyakOrang) BanyakOrang
	FROM ctePeak cteP
	GROUP BY
		cteP.Jam

		SELECT *
FROM vwPeak vP
WHERE
	vP.BanyakOrang = (SELECT MAX(BanyakOrang) FROM vwPeak)