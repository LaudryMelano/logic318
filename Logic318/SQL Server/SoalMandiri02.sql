--1. Tampilkan nama barang, harga barang dan tambahkan kolom dengan keterangan
--   jika diatas rata2 maka upper, jika dibawah rata2 maka lower

select 
	Nama, Harga, 
	case when Harga>(select avg(Harga) from Product) then 'Upper' 
	else 'Lower' end Keterangan
from
	Product
order by Keterangan DESC, Harga DESC, Nama

-- 2. Tampilkan Produk yang terjual di bulan dan hari yang sama
	SELECT pro.Nama, dayPro.Bulan, dayPro.Hari
	FROM vwDayMonthProduk dayPro
	JOIN Product pro ON dayPro.KodeProduct = pro.Kode
	JOIN
		(
			SELECT *
			FROM vwDayMonthProduk dp
			JOIN Product pro ON dp.KodeProduct = pro.Kode
		) sub
	ON dayPro.Hari = sub.Hari AND dayPro.Bulan = sub.Bulan AND dayPro.KodeProduct <> sub.KodeProduct
	ORDER BY dayPro.Bulan

CREATE VIEW vwDayMonthProduk
AS
SELECT KodeProduct, DAY(SellingDate) Hari, MONTH(SellingDate) Bulan 
FROM 
Selling

-- 3. Tampilkan Frekuensi terjualnya suatu produk dengan menampilkan tanggal dan quantitynya
SELECT KodeProduct,SUM(Quantity) Quantity,DATENAME(DAY,SellingDate) Tanggal, COUNT(DAY(SellingDate))Kali_Terjual
FROM Selling
GROUP BY KodeProduct, DATENAME(DAY,SellingDate)
ORDER BY Quantity,DATENAME(DAY,SellingDate) DESC

--GROUP 03
-- 1. Tampilkan Kode Produk, Nama Produk dan  jumlah produk diatas rata-rata yang nama produknya berakhiran i dan mengubah Nama Produk menjadi huruf kapital

SELECT sel.KodeProduct, UPPER(pro.Nama), SUM(sel.Quantity) JumlahProduk
FROM Selling sel
JOIN Product pro ON sel.KodeProduct = pro.Kode
GROUP BY sel.KodeProduct, pro.Nama
HAVING SUM(sel.Quantity) > (SELECT AVG(sub.Quantity) FROM (SELECT SUM(Quantity) Quantity FROM Selling GROUP BY KodeProduct)sub) AND pro.Nama LIKE '%i'


-- 2. Tampilkan NamaProduk, Harga, dan JumlahTerjual, untuk produk yang belum pernah laku terjual

SELECT
pro.Nama, pro.Harga,
	CASE 
		WHEN SUM(sel.Quantity) IS NULL THEN '0'
	END JumlahTerjual
FROM Selling sel
RIGHT JOIN Product pro ON sel.KodeProduct = pro.Kode
GROUP BY  pro.kode, pro.Harga, pro.Nama
HAVING  SUM(sel.Quantity) IS NULL


-- 3. Tampilkan NamaKota dan JumlahTransaksi, untuk kota yang belum pernah terjadi transaksi
SELECT kt.Nama, COUNT(sel.Referensi) JumlahTransaksi
FROM Selling sel
JOIN Outlet ot ON sel.KodeOutlet = ot.Kode
RIGHT JOIN Kota kt ON ot.KodeKota = kt.Kode
GROUP BY kt.Kode, kt.Nama
HAVING COUNT(sel.Referensi) = 0

--Group 01
--1. Tampilkan hasil penjualan Outlet Jaktim Jaya berdasarkan hari di tahun 2022
select*from Outlet
select 
	day(sel.SellingDate) Hari, cast(pro.Harga*sel.Quantity as int) HasilPenjualan
from Product pro
	join Selling sel on sel.KodeProduct=pro.Kode
	join Outlet ol on ol.Kode=sel.KodeOutlet
where ol.KodeKota='K1003' and year(sel.SellingDate)=2022
order by day(sel.SellingDate)

--2. Tampilkan nama produk & jumlah penjualan produk dari outlet Jakpus Jaya

select
	pro.Nama NamaProduk, cast(sum(pro.Harga*sel.Quantity) as money) JumlahPenjualan
from 
	Product pro
	join Selling sel on sel.KodeProduct=pro.Kode
	join Outlet ol on ol.Kode=sel.KodeOutlet
where
	ol.KodeKota='K1001'
group by
	pro.Nama

--3. Tampilkan nama produk yang terjual di bulan Januari

select
	 year(sel.SellingDate) Tahun, month(sel.SellingDate) Bulan,pro.Nama [Nama Produk]
from 
	Product pro
	join Selling sel on sel.KodeProduct=pro.Kode
where
	month(sel.SellingDate)=1

---- Group 04
--1. tampilkan harga produk termurah perprovinsi
SELECT pmhPro.NamaProv, pmhPro.NamaProd, pmhPro.Harga 
FROM ProvMinHarga pmhPro
	JOIN 
(
		SELECT NamaProv, MIN(Harga) Harga
		FROM ProvMinHarga
		GROUP BY NamaProv
) 
pmh ON pmhPro.NamaProv = pmh.NamaProv AND pmhPro.Harga = pmh.Harga

ALTER VIEW ProvMinHarga
AS
SELECT Prov.Nama NamaProv,prod.Nama NamaProd,MIN(Harga) Harga
FROM Selling sell
JOIN Product prod on prod.Kode = sell.KodeProduct
JOIN Outlet  outl on outl.Kode = sell.KodeOutlet
JOIN Kota on Kota.Kode = outl.KodeKota
JOIN Provinsi prov on prov.Kode = Kota.KodeProvinsi
GROUP BY prov.Nama,prod.Nama
--2. tampilkan outlet yg menjual (roti, pasta gigi dan seblak) dengan nama produk,quantity, harga & jumlah penjualan
SELECT outl.Kode,outl.Nama,prod.Nama,SUM(Quantity) Quantity,Harga,SUM((Harga*Quantity)) JumlahPenjualan
FROM Selling sell
JOIN Product prod on prod.Kode = sell.KodeProduct
JOIN Outlet  outl on outl.Kode = sell.KodeOutlet
WHERE prod.Nama IN ('Roti','Pasta gigi','Seblak') 
GROUP BY outl.Kode,outl.Nama,prod.Nama,Harga
--3. Buat Nama untuk outlet Daerah Jakarta yang belum memiliki outlet
SELECT
Kota.Nama NamaKota, CONCAT(SUBSTRING(Kota.Nama, 1,3),SUBSTRING(Kota.Nama, 9,3))
FROM Kota
LEFT JOIN Outlet outl ON Kota.Kode = outl.KodeKota
WHERE outl.Nama IS NULL AND Kota.Nama LIKE '%Jakarta%'

select * from Product
