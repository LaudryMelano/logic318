--SQL DAY 01--

CREATE DATABASE DbMahasiswa318

use DbMahasiswa318

INSERT INTO Jurusan (Kode_Jurusan, Nama_Jurusan, Status_Jurusan)
VALUES 
('J001', 'Teknik Informatika', 'Aktif'),
('J002', 'Management Informatika', 'Aktif'),
('J003', 'Sistem Informasi', 'Non Aktif'),
('J004', 'Sistem Komputer', 'Aktif'),
('J005', 'Komputer Akuntansi', 'Non Aktif')

SELECT * FROM Jurusan

CREATE TABLE Agama (
Id int identity (1,1) not null,
Kode_Agama char(5) primary key,
Deskripsi varchar(20)
)

INSERT INTO Agama (Kode_Agama, Deskripsi)
VALUES
('A001', 'Islam'),
('A002', 'Kristen'),
('A003', 'Katolik'),
('A004', 'Hindu'),
('A005', 'Budah')

CREATE TABLE Mahasiswa (
Id int identity(1,1) not null,
Kode_Mahasiswa char(5) primary key,
Nama_Mahasiswa varchar(100) not null,
Alamat varchar(200) not null,
Kode_Agama char(5) not null,
Kode_Jurusan char(5) not null
)

INSERT INTO Mahasiswa(Kode_Mahasiswa, Nama_Mahasiswa, Alamat, Kode_Agama, Kode_Jurusan)
VALUES
('M001', 'Budi Gunawan', 'Jl. Mawar No 3 RT 05 Cicalengka, Bandung', 'A001', 'J001'),
('M002', 'Rinto Raharjo', 'Jl. Kebagusan No. 33 RT04 RW06 Bandung', 'A002', 'J002'),
('M003', 'Asep Saepudin', 'Jl. Sumatera No. 12 RT02 RW01, Ciamis', 'A001', 'J003'),
('M004', 'M. Hafif Isbullah', 'Jl. Jawa No 01 RT01 RW01, Jakarta Pusat', 'A001', 'J001'),
('M005', 'Cahyono', 'Jl. Niagara No. 54 RT01 RW09, Surabaya', 'A003', 'J002')

CREATE TABLE Ujian(
Id int identity(1,1) not null,
Kode_Ujian char(5) primary key not null,
Nama_Ujian varchar(50) not null,
Status_Ujian varchar(100) not null
)

INSERT INTO Ujian(Kode_Ujian, Nama_Ujian, Status_Ujian)
VALUES
('U001', 'Algoritma', 'Aktif'),
('U002', 'Aljabar', 'Aktif'),
('U003', 'Statistika', 'Non Aktif'),
('U004', 'Etika Profesi', 'Non Aktif'),
('U005', 'Bahasa Inggris', 'Aktif')

CREATE TABLE Type_Dosen(
Id int identity(1,1) not null,
Kode_TypeDosen char(5) primary key not null,
Deskripsi varchar(20)
)

INSERT INTO Type_Dosen(Kode_TypeDosen, Deskripsi)
VALUES 
('T001', 'Tetap'),
('T002', 'Honorer'),
('T003', 'Expertise')

CREATE TABLE Dosen(
Id int identity(1,1) not null,
Kode_Dosen char(5) primary key,
Nama_Dosen varchar(100),
Kode_Jurusan char(5),
Kode_Type_Dosen char(5)
)

INSERT INTO Dosen(Kode_Dosen, Nama_Dosen, Kode_Jurusan, Kode_Type_Dosen)
VALUES
('D001', 'Prof. Dr. Retno Wahyuningsih', '	J001', 'T002'),
('D002', 'Prof. Roy M. Sutikno', '	J002', 'T001'),
('D003', 'Prof. Hendri A. Verburgh', '	J003', 'T002'),
('D004', 'Prof. Risma Suparwata', '	J004', 'T002'),
('D005', 'Prof. Amir Sjarifuddin Madjid, MM, MA', '	J005', 'T001')

CREATE TABLE Nilai(
Id int primary key identity(1,1) not null,
Kode_Mahasiswa char(5),
Kode_Ujian char(5),
Nilai decimal(8,2)
)

INSERT INTO Nilai (Kode_Mahasiswa, Kode_Ujian, Nilai)
VALUES
('M004', 'U001', 90),
('M001', 'U001', 80),
('M002', 'U003', 85),
('M004', 'U002', 95),
('M005', 'U005', 70)


---- No.2 ------
ALTER TABLE Dosen ALTER COLUMN Nama_Dosen varchar(200)

---- No.3 ------
--Buatlah query untuk menampilkan data berikut:
--Kode_Mahasiswa	Nama_Mahasiswa	Nama_Jurusan	Agama			
--M001	Budi Gunawan	Teknik Informatika	Islam

SELECT Kode_Mahasiswa, Nama_Mahasiswa, nama_jurusan, Deskripsi 
FROM Mahasiswa m
JOIN Agama a ON m.Kode_Agama = a.Kode_Agama
JOIN Jurusan j ON j.kode_jurusan = m.Kode_Jurusan
WHERE Kode_Mahasiswa = 'M001'

---- No.4 -----
-- Buatlah query untuk menampilkan data mahasiswa yang mengambil jurusan dengan Status Jurusan = Non Aktif
SELECT Kode_Mahasiswa, Nama_Mahasiswa, alamat, nama_jurusan, status_jurusan
FROM Mahasiswa m
JOIN Jurusan j ON m.Kode_Jurusan = j.kode_jurusan
WHERE j.status_jurusan = 'Non AKtif'

---- No.5 -----
-- Buatlah query untuk menampilkan data mahasiswa dengan nilai diatas 80 untuk ujian dengan Status Ujian = Aktif
SELECT  m. *, nilai, status_ujian   
FROM Mahasiswa m
JOIN Nilai n ON m.Kode_Mahasiswa = n.Kode_Mahasiswa
JOIN Ujian u ON n.Kode_Ujian = u.Kode_Ujian
WHERE Nilai > 80 AND status_ujian = 'Aktif' 

---- No.6 ----
-- Buatlah query untuk menampilkan data jurusan yang mengandung kata 'sistem'.
SELECT *
FROM Jurusan
WHERE nama_jurusan LIKE 'Sistem%'

---- No.7 ----
-- Buatlah query untuk menampilkan mahasiswa yang mengambil ujian lebih dari 1.
SELECT  m.Nama_Mahasiswa
FROM Mahasiswa m
JOIN Nilai n ON m.Kode_Mahasiswa = n.Kode_Mahasiswa
JOIN Ujian u ON n.Kode_Ujian = u.Kode_Ujian
GROUP BY m.Nama_Mahasiswa
HAVING count(m.Kode_Mahasiswa)>1

--- No. 8 -----
-- Buatlah query untuk menampilkan data seperti berikut:
-- Kode_Mahasiswa	Nama_Mahasiswa	Nama_Jurusan	Agama	Nama_Dosen	Status_Jurusan	Deskripsi
-- M001	Budi Gunawan	Teknik Informatika	Islam	Prof. Dr. Retno Wahyuningsih	Aktif	Honorer

SELECT
	m.Kode_Mahasiswa,
	m.Nama_Mahasiswa,
	j.Nama_Jurusan,
	a.Deskripsi Agama,
	d.Nama_Dosen,
	j.Status_Jurusan,
	td.Deskripsi
FROM dbo.Mahasiswa m
	JOIN dbo.Jurusan j 
		on j.Kode_Jurusan = m.Kode_Jurusan
	JOIN dbo.Dosen d 
		on d.Kode_Jurusan = j.Kode_Jurusan
	JOIN dbo.Agama a 
		on a.Kode_Agama = m.Kode_Agama
	JOIN dbo.Type_Dosen td 
		on td.Kode_TypeDosen = d.Kode_Type_Dosen
WHERE
	m.Kode_Mahasiswa = 'M001'

SELECT m.Kode_Mahasiswa, m.Nama_Mahasiswa, j.nama_jurusan, a.Deskripsi Agama, d.Nama_Dosen, j.status_jurusan, td.Deskripsi
FROM Mahasiswa m
JOIN Jurusan j ON m.Kode_Jurusan = j.kode_jurusan
JOIN Agama a ON m.Kode_Agama = a.Kode_Agama
JOIN Dosen d ON d.Kode_Jurusan = j.kode_jurusan
JOIN Type_Dosen td ON td.Kode_TypeDosen = d.Kode_Type_Dosen
WHERE m.Kode_Mahasiswa = 'M001'


--- No. 9 ----
--	Buatlah query untuk create view dengan menggunakan data pada no. 8, beserta query untuk mengeksekusi view tersebut.
CREATE VIEW vwMahasiswa
AS
SELECT m.Kode_Mahasiswa, m.Nama_Mahasiswa, j.nama_jurusan, a.Deskripsi As agama, d.Nama_Dosen, j.status_jurusan, td.Deskripsi
FROM Mahasiswa m
JOIN Jurusan j ON m.Kode_Jurusan = j.kode_jurusan
JOIN Agama a ON m.Kode_Agama = a.Kode_Agama
JOIN Dosen d ON d.Kode_Jurusan = j.kode_jurusan
JOIN Type_Dosen td ON td.Kode_TypeDosen = d.Kode_Type_Dosen
WHERE m.Kode_Mahasiswa = 'M001'
SELECT * FROM vwMahasiswa

--- No. 10 ----
-- Buatlah query untuk menampilkan data mahasiswa beserta nilainya (mahasiswa yang tidak punya nilai juga ditampilkan).
SELECT  m.Nama_Mahasiswa, n.Nilai
FROM  Nilai n RIGHT JOIN Mahasiswa m ON m.Kode_Mahasiswa = n.Kode_Mahasiswa

--- No. 11 ----
--Buatlah query untuk menampilkan data mahasiswa beserta nilainya yang memiliki nilai:
--a. Minimum
--b. Maximum
--c. Diatas rata-rata
--d. Dibawah rata-rata

--a
SELECT TOP 1 m. *, n.Nilai
FROM Nilai n
JOIN Mahasiswa m ON m.Kode_Mahasiswa = n.Kode_Mahasiswa
ORDER BY n.Nilai ASC

SELECT m.*, n.Nilai
FROM mahasiswa m
join Nilai n on m.Kode_Mahasiswa = n.Kode_Mahasiswa 
WHERE nilai = (SELECT MIN(Nilai) FROM Nilai)

--b
SELECT TOP 1 m. *, n.Nilai
FROM Nilai n
JOIN Mahasiswa m ON m.Kode_Mahasiswa = n.Kode_Mahasiswa
ORDER BY n.Nilai DESC

SELECT m.*, n.Nilai
FROM mahasiswa m
join Nilai n on m.Kode_Mahasiswa = n.Kode_Mahasiswa 
WHERE nilai = (SELECT MAX(Nilai) FROM Nilai)

--c
SELECT m. *, n.Nilai
FROM mahasiswa m
join Nilai n on m.Kode_Mahasiswa = n.Kode_Mahasiswa 
WHERE nilai > (SELECT AVG(Nilai) FROM Nilai)

--d
SELECT m. *, n.Nilai
FROM mahasiswa m
join Nilai n on m.Kode_Mahasiswa = n.Kode_Mahasiswa 
WHERE nilai < (SELECT AVG(Nilai) FROM Nilai)

--- No. 12 ---
-- Tambahkan kolom Biaya dengan type data Money/Number/Decimal 18,4 pada table Jurusan
ALTER TABLE Jurusan ADD biaya decimal(18,4)

--- No.13 ---
UPDATE jurusan SET biaya = 1500000	WHERE nama_jurusan='J001'
UPDATE jurusan SET biaya = 1550000	WHERE nama_jurusan='J002'	
UPDATE jurusan SET biaya = 1475000	WHERE nama_jurusan='J003'
UPDATE jurusan SET biaya = 1350000	WHERE nama_jurusan='J004'
UPDATE jurusan SET biaya = 1535000	WHERE nama_jurusan='J005'

--- No.14 ---
SELECT nama_jurusan, biaya
FROM jurusan
WHERE biaya = (SELECT MIN(biaya) FROM jurusan)

SELECT nama_jurusan, biaya
FROM jurusan
WHERE biaya = (SELECT MAX(biaya) FROM jurusan)

SELECT nama_jurusan, biaya
FROM jurusan
WHERE biaya > (SELECT AVG(biaya) FROM jurusan)

SELECT nama_jurusan, biaya
FROM jurusan
WHERE biaya < (SELECT AVG(biaya) FROM jurusan)














