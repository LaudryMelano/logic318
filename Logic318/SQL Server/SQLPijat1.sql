create database PijatTerserah
use PijatTerserah
drop database PijatTerserah

create table Grade(ID_Grade varchar(5) primary key,
type varchar(15) not null);

create table Provinsi(ID_Provinsi varchar(5) primary key,
Nama varchar(35) not null);

create table Kota(ID_Kota varchar(5) primary key,
Nama varchar(35) not null, ID_Provinsi varchar(5) foreign key references Provinsi(ID_Provinsi));

create table Member(ID_Member varchar(5) primary key,
Nama_Depan varchar(25) not null, Nama_Belakang varchar(35) null,
Alamat varchar(500) null, ID_Kota varchar(5) foreign key references Kota(ID_Kota),
ID_Grade varchar(5) foreign key references Grade(ID_Grade));

create table Job_Title(ID_Job varchar(5) primary key,
Title varchar(25) not null);

create table Employee(ID_Employee varchar(5) primary key,
Nama_Depan varchar(25) not null, Nama_Belakang varchar(35) null,
ID_Job varchar(5) foreign key references Job_Title(ID_Job));

create table Booking(ID_Booking varchar(4) primary key,
Tanggal DateTime not null,
ID_Member varchar(5) foreign key references Member(ID_Member));

--create table Type_Message(ID_Type_Message varchar(5) primary key,
--ID_Booking varchar(4) foreign key references Booking(ID_Booking),
--Tipe varchar(25) not null);

create table Massage(ID_Massage varchar(5) primary key,
Tipe varchar(25) not null, Harga money not null);

create table Goods(ID_Goods varchar(5) primary key,
Tipe varchar(25) not null,Quantity int not null, Harga money not null);

create table BookingMessage(ID_Booking_Message varchar(5) primary key,
ID_Booking varchar(4) foreign key references Booking(ID_Booking),
ID_Massage varchar(5) foreign key references Massage(ID_Massage),
Masanger varchar(5) foreign key references Employee(ID_Employee),
Harga money not null);

create table BookingGoods(ID_Booking_Goods varchar(5) primary key,
ID_Booking varchar(4) foreign key references Booking(ID_Booking),
ID_Goods varchar(5) foreign key references Goods(ID_Goods),
Quantity int not null, Harga money not null);

create table Invoice(ID_Invoice varchar(9) primary key,
Tanggal_Invoice datetime not null,
ID_Booking varchar(4) foreign key references Booking(ID_Booking),
Kasir varchar(5) foreign key references Employee(ID_Employee));

insert into Provinsi(ID_Provinsi,Nama)
values('PV001','Banten');

insert into Kota(ID_Kota, Nama, ID_Provinsi)
values('KT001','Tanggerang Selatan','PV001');

insert into Grade(ID_Grade,[type])
values('GD001','Gold');

insert into Member(ID_Member, Nama_Depan, Nama_Belakang, Alamat, ID_Kota, ID_Grade)
values('M0982','Bayu',null,'Jl. Garuda','KT001','GD001');

insert into Job_Title(ID_Job, Title)
values('JB001','Massager'),('JB002','Kasir');

insert into Employee(ID_Employee, Nama_Depan, Nama_Belakang,ID_Job)
values('E0001','Wahyu',null,'JB001'),('E0002','Nino',null,'JB002');

insert into Booking(ID_Booking, Tanggal, ID_Member)
values('B098','20230104','M0982');

insert into Massage(ID_Massage, Tipe, Harga)
values('M0001','Full Body',120000);

insert into BookingMessage(ID_Booking_Message, ID_Booking, ID_Massage, Masanger, Harga)
values('BM001','B098','M0001','E0001',120000);

insert into Goods(ID_Goods, Tipe, Quantity, Harga)
values('G0001','Miyak Terapi',60,25000),('G0002','Wedang Jahe',100,15000),
('G0003','Handuk',120,46500);

insert into BookingGoods(ID_Booking_Goods, ID_Booking, ID_Goods, Quantity, Harga)
values('BG001','B098','G0001',1,25000),('BG002','B098','G0002',1,150000),
('BG003','B098','G0003',1,46500);

insert into Invoice(ID_Invoice, Tanggal_Invoice, ID_Booking, Kasir)
values('M23010923','20230105','B098','E0002');

select inv.Kasir, bm.Masanger, 'Massage'as [msg.tipe]
from Invoice inv
join Booking bk on inv.ID_Booking = bk.ID_Booking
join BookingMessage bm on bk.ID_Booking = bm.ID_Booking
join Massage msg on bm.ID_Massage = msg.ID_Massage
join Member mbr on bk.ID_Member = mbr.ID_Member
join kota kt on mbr.ID_Kota = kt.ID_Kota
join Provinsi prv on kt.ID_Provinsi = prv.ID_Provinsi
join Grade grd on mbr.ID_Grade = grd.ID_Grade
join Employee emp on bm.Masanger = emp.ID_Employee 
join Job_Title jb on emp.ID_Job = jb.ID_Job

create view DataGathering
as
select iv.Tanggal_Invoice [Date],iv.Kasir, iv.ID_Invoice [Reff], bo.ID_Booking [Book No], bo.Tanggal [Tgl], 
mem.ID_Member [MemberId],mem.Nama_Depan [NamaCustomer],mem.Alamat [Alamat], kt.Nama [Kota], prov.Nama [Provinsi], gd.[type] [Grade], 
'Message' as [type], mas.Tipe [detailType], em.Nama_Depan [Masanger], boma.Harga [Price]
from Invoice iv
join Booking bo on iv.ID_Booking=bo.ID_Booking
join BookingMessage boma on bo.ID_Booking=boma.ID_Booking 
join Massage mas on mas.ID_Massage=boma.ID_Massage
join Employee em on boma.Masanger=em.ID_Employee
join Job_Title joti on joti.ID_Job=em.ID_Job
join Member mem on mem.ID_Member=bo.ID_Member
join Grade gd on gd.ID_Grade=mem.ID_Grade
join Kota kt on kt.ID_Kota=mem.ID_Kota
join Provinsi prov on prov.ID_Provinsi=kt.ID_Provinsi
union 
select iv.Tanggal_Invoice [Date],iv.Kasir, iv.ID_Invoice [Reff], bo.ID_Booking [Book No], bo.Tanggal [Tgl], 
mem.ID_Member [MemberId],mem.Nama_Depan [NamaCustomer],mem.Alamat [Alamat], kt.Nama [Kota], prov.Nama [Provinsi],gd.[type] [Grade],
'Goods' as [type], god.Tipe [detailType], '' as [Masanger],bogo.Harga [Price]
from Invoice iv
join Booking bo on iv.ID_Booking=bo.ID_Booking
join BookingGoods bogo on bo.ID_Booking=bogo.ID_Booking
join Goods god on god.ID_Goods=bogo.ID_Goods
join Member mem on mem.ID_Member=bo.ID_Member
join Grade gd on gd.ID_Grade=mem.ID_Grade
join Kota kt on kt.ID_Kota=mem.ID_Kota
join Provinsi prov on prov.ID_Provinsi=kt.ID_Provinsi

select [Date],Reff, [Book No], Tgl, MemberId,NamaCustomer,Alamat, Kota, Provinsi,Grade,
[type], detailType, Masanger,Price,em.Nama_Depan as Kasir
from DataGathering dg
join Employee em on em.ID_Employee=dg.Kasir



select iv.Tanggal_Invoice, iv.ID_Invoice, bo.ID_Booking, bo.Tanggal, mem.ID_Member, 
mem.Nama_Depan, mem.Alamat, kt.Nama, prov.Nama, gd.[type]
from Invoice iv
full join Booking bo on iv.ID_Booking=bo.ID_Booking
full join BookingMessage boma on bo.ID_Booking=boma.ID_Booking 
full join Massage mas on mas.ID_Massage=boma.ID_Massage
full join BookingGoods bogo on bo.ID_Booking=bogo.ID_Booking
full join Goods god on god.ID_Goods=bogo.ID_Goods

full join Employee em on iv.Kasir=em.ID_Employee and boma.Masanger=em.ID_Employee
full join Job_Title joti on joti.ID_Job=em.ID_Job

full join Member mem on mem.ID_Member=bo.ID_Member
full join Grade gd on gd.ID_Grade=mem.ID_Grade
full join Kota kt on kt.ID_Kota=mem.ID_Kota
full join Provinsi prov on prov.ID_Provinsi=kt.ID_Provinsi

full join 