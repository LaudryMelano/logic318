
--01 Tapilkan jumlah penjualan barang peroutlet per-tanggal
Select pro.nama, SUM(sel.quantity) [Jumlah Penjualan], sel.sellingdate, ot.nama 
FROM Selling sel
JOIN Product pro on pro.kode = sel.kodeproduct
JOIN outlet ot on ot.kode = sel.kodeoutlet 
group by sel.sellingdate, ot.nama, pro.nama
order by sel.sellingdate

--02 Tapilkan jumlah penjualan per tahun
SELECT SUM(quantity) AS 'Jumlah Penjualan',
	year(sellingdate) as 'Tahun'
FROM selling
group by year(sellingdate) 

--03 Tapilkan jumlah product terlaris dan ter tidak laris per kota
SELECT
t1.NamaKota,t1.KodeKota,
Min(t1.[Quantity]) Tertidaklaris,
Max(t1.[Quantity]) Terlaris
FROM
(
	select k.nama NamaKota, k.kode KodeKota, sel.kodeproduct KodeProduct, sum(sel.quantity) Quantity
	from
	selling sel 
	join outlet ot on sel.kodeoutlet = ot.kode
	join kota k on ot.kodekota = k.kode
	group by k.nama, sel.kodeproduct, k.kode
) t1
group by t1.KodeKota,t1.NamaKota

--04 Tapilkan jumlah penjualan per provinsi dan urutkan dari yang terbesar
SELECT prv.Nama Provinsi, SUM(sel.Quantity*pro.Harga) JumlahPenjualan
FROM Selling sel
JOIN Product pro ON pro.kode = sel.KodeProduct
JOIN Outlet ot ON ot.kode = sel.KodeOutlet
JOIN Kota kt ON kt.Kode = ot.KodeKota
JOIN Provinsi prv ON prv.Kode = kt.KodeProvinsi
GROUP BY prv.Nama
ORDER BY JumlahPenjualan Desc


--05 Tampilkaan referensi yang tidak sesuai dengan sellingdate
SELECT Reference, SellingDate
FROM 
(SELECT Referensi Reference, SellingDate,YEAR(SellingDate) SelYear, MONTH(SellingDate) SelMonth, CAST(SUBSTRING(Referensi, 6,2)as int) dtRef, CAST(SUBSTRING(Referensi, 4,2)as int) yrRef FROM Selling) sel
WHERE  sel.SelMonth <> sel.dtRef AND sel.SelYear <> yrRef

--- ATAU ---

SELECT Referensi,YEAR(SellingDate) Tahun,MONTH(SellingDate) Bulan
FROM Selling sell
WHERE CAST(SUBSTRING(Referensi, 4,2)as int) <> YEAR(SellingDate) 
AND CAST(SUBSTRING(Referensi, 6,2)as int) <> MONTH(SellingDate)

SELECT Referensi
FROM Selling


-- 06 Tampilkann jumlah produk terjual pertahun peroutlet
SELECT 
ot.Nama NamaOutlet, 
YEAR(sel.sellingdate) Tahun,
SUM(sel.quantity) JumlahPenjualan
FROM selling sel
JOIN Outlet ot ON sel.KodeOutlet = ot.Kode
GROUP BY YEAR(sel.sellingdate), ot.Nama

-- 07 Tampilkan jumlah penjualan peroutlet
SELECT 
ot.Nama NamaOutlet, 
SUM(sel.quantity*pro.Harga) JumlahPenjualan
FROM selling sel
JOIN Outlet ot ON sel.KodeOutlet = ot.Kode
JOIN Product pro ON pro.kode = sel.KodeProduct
GROUP BY ot.Kode,ot.Nama

--08 Tampilkan jumlah penjualan per periode per bulan diurutkan berdasar bulan
SELECT
YEAR(sel.sellingdate) Periode,
DATENAME(month, sel.sellingdate) Bulan,
SUM(sel.quantity*pro.Harga) JumlahPenjualan
FROM selling sel
JOIN Product pro ON pro.kode = sel.KodeProduct
GROUP BY YEAR(sel.sellingdate), DATENAME(month, sel.sellingdate),MONTH(sel.sellingdate)
ORDER BY YEAR(sel.sellingdate), MONTH(sel.sellingdate)

SELECT Distinct *
FROM vwDayMonthProduk dayPro
JOIN
	(
		SELECT *
		FROM vwDayMonthProduk
	) sub
ON dayPro.Hari = sub.Hari AND dayPro.Bulan = sub.Bulan AND dayPro.KodeProduct <> sub.KodeProduct

CREATE VIEW vwDayMonthProduk
AS
SELECT KodeProduct, DAY(SellingDate) Hari, MONTH(SellingDate) Bulan 
FROM 
Selling

--09 Tampilkan rata-rata jumlah penjualan setiap bulan
SELECT 
DATENAME(month, sel.sellingdate) Bulan,
AVG(sel.quantity*pro.Harga) RataRataPenjualan
FROM selling sel
JOIN Product pro ON pro.kode = sel.KodeProduct
GROUP BY DATENAME(month, sel.sellingdate),MONTH(sel.sellingdate)
ORDER BY MONTH(sel.sellingdate)

--10 Tampiklan produk dengan jumlah produk dibawah rata-rata
SELECT 
pro.nama,
SUM(sel.quantity*pro.harga) JumlahPenjualanProduk
FROM selling sel
JOIN Product pro ON pro.kode = sel.KodeProduct
GROUP BY pro.nama
HAVING SUM(sel.quantity*pro.harga) > (SELECT AVG(sel.Quantity*pro.harga) FROM Selling sel JOIN Product pro ON pro.kode = sel.KodeProduct)

SELECT 
pro.nama,
SUM(sel.quantity) JumlahProdukTerjual
FROM selling sel
JOIN Product pro ON pro.kode = sel.KodeProduct
GROUP BY pro.nama
HAVING SUM(sel.quantity) > (SELECT AVG(sel.Quantity) FROM Selling sel)

SELECT
	p.Nama NamaProduk,
	sum(Quantity) JumlahTerjual
FROM
	dbo.Selling s
	JOIN dbo.Product p
		ON p.Kode = s.KodeProduct
GROUP BY
	p.Kode,
	p.Nama
HAVING
	sum(Quantity) < (SELECT avg(y.SumQuantity) 
	FROM (SELECT KodeProduct, sum(Quantity) SumQuantity 
	FROM dbo.Selling GROUP BY KodeProduct) y)

--11 Tampilan jumlah outlet per provinsi
SELECT prv.Nama Provinsi, COUNT(ot.Kode) JumlahOutlet
FROM Outlet ot
JOIN Kota kt ON kt.Kode = ot.KodeKota
JOIN Provinsi prv ON prv.Kode = kt.KodeProvinsi
GROUP BY prv.Kode, prv.Nama

--12 Tampilan produk terlaris per perionde bulanan

SELECT
sub.Periode, sub.Bulan,
sub.bln,
Max(sub.Quantity) Terlaris
FROM
(
	select pro.Nama Product, sel.kodeproduct, sum(sel.quantity) Quantity, YEAR(sel.sellingdate) Periode, MONTH(sel.sellingdate) bln,
	DATENAME(month, sel.sellingdate) Bulan
	from
	selling sel 
	join Product pro ON sel.KodeProduct = pro.Kode
	group by sel.kodeproduct, pro.Nama, YEAR(sel.sellingdate), DATENAME(month, sel.sellingdate),MONTH(sel.sellingdate)
) sub
GROUP BY sub.Periode, sub.bln, sub.Bulan
ORDER BY sub.Periode, sub.bln

SELECT SelQty.*
FROM SelQty
JOIN 
(
	SELECT [Year], [Month], MAX(Quantity) Quantity
	FROM SelQty
	GROUP BY [Year], [Month]
) SelMaxQty
ON SelQty.Quantity = SelMaxQty.Quantity AND SelQty.Year = SelMaxQty.Year AND SelQty.Month = SelMaxQty.Month
ORDER BY SelQty.Year, SelQty.Month

CREATE VIEW SelQty
AS
SELECT YEAR(SellingDate) [YEAR], MONTH(SellingDate) [Month],
KodeProduct, SUM(Quantity) Quantity
FROM Selling
GROUP BY YEAR(SellingDate), MONTH(SellingDate), KodeProduct

--13 Tampilkan provinsi yg menjual sampoo, quantity, harga & jumlah penjualan
select 
    prov.Nama,
	sum(sell.Quantity) Quantity,
	pro.Harga,
	sum(sell.Quantity * Harga) JumlahPenjualan
from Selling sell
	join Product pro
		on sell.KodeProduct = pro.Kode 
	join outlet ou
		on ou.Kode = sell.KodeOutlet
	join Kota ko
		on ou.KodeKota = ko.Kode
	join Provinsi prov
		on prov.Kode = ko.KodeProvinsi
where pro.Nama = 'Sampoo'
group by  prov.Nama, pro.Harga

--14 Tampilkan produk harga termahal setiap provinsi

SELECT pmhPro.NamaProv,  pmhPro.NamaProd, pmhPro.Harga
FROM ProvMaxHarga pmhPro
	JOIN
	(
		SELECT KodeProv, NamaProv, MAX(Harga) Harga
			FROM ProvMaxHarga
			GROUP BY KodeProv, NamaProv
	) pmh
	ON pmhPro.KodeProv = pmh.KodeProv AND pmhPro.Harga = pmh.Harga

CREATE VIEW ProvMaxHarga
AS
SELECT prv.Kode KodeProv, prv.Nama NamaProv,
	pro.Kode KodeProd, pro.Nama NamaProd,
	MAX(Harga) Harga
FROM Selling sel
	JOIN Outlet Ou ON Ou.Kode = sel.KodeOutlet
	JOIN Product pro ON sel.KodeProduct = pro.Kode
	JOIN Kota kot ON Ou.KodeKota = kot.Kode
	JOIN Provinsi prv ON kot.KodeProvinsi = prv.Kode
GROUP BY prv.Kode, prv.Nama, pro.Kode, pro.Nama

--15 Tampilkan outlet dengan penjualan tertinggi & terendah
SELECT
		CASE
			WHEN sm.Penjualan = (SELECT * FROM max15) THEN 'Maksimal'
			WHEN sm.Penjualan = (SELECT * FROM min15) THEN 'Minimal'
		END Stat,
		sm.NamaO NamaOutlet,
		sm.Penjualan
	FROM (
		SELECT
			o.Kode KodeO,
			o.Nama NamaO,
			--prod.Kode KodeP,
			SUM(s.Quantity * prod.Harga) Penjualan
		FROM dbo.Selling s
		JOIN dbo.Outlet o
			ON o.Kode = s.KodeOutlet
		JOIN dbo.Product prod
			ON prod.Kode = s.KodeProduct
		GROUP BY
			o.Kode,
			o.Nama
	) sm
	WHERE
		sm.Penjualan = (SELECT * FROM max15) OR
		sm.Penjualan = (SELECT * FROM min15)
		--sm.Penjualan = @max OR
		--sm.Penjualan = @min

CREATE VIEW max15
AS
	SELECT
		MAX(mx.Penjualan) Penjualan
	FROM (
		SELECT
			o.Kode KodeO,
			--prod.Kode KodeP,
			SUM(s.Quantity * prod.Harga) Penjualan
		FROM dbo.Selling s
		JOIN dbo.Outlet o
			ON o.Kode = s.KodeOutlet
		JOIN dbo.Product prod
			ON prod.Kode = s.KodeProduct
		GROUP BY
			o.Kode
	) mx

CREATE VIEW min15
AS
	SELECT
		MIN(mn.Penjualan) Penjualan
	FROM (
		SELECT
			o.Kode KodeO,
			--prod.Kode KodeP,
			SUM(s.Quantity * prod.Harga) Penjualan
		FROM dbo.Outlet o
		LEFT JOIN dbo.Selling s
			ON o.Kode = s.KodeOutlet
		LEFT JOIN dbo.Product prod
			ON prod.Kode = s.KodeProduct
		GROUP BY
			o.Kode
	) mn
--15 Tampilkan outlet dengan penjualan tertinggi & terendah
SELECT
		CASE
			WHEN sm.Penjualan = (SELECT * FROM max15) THEN 'Maksimal'
			WHEN sm.Penjualan = (SELECT * FROM min15) THEN 'Minimal'
		END Stat,
		sm.NamaO NamaOutlet,
		sm.Penjualan
	FROM (
		SELECT
			o.Kode KodeO,
			o.Nama NamaO,
			--prod.Kode KodeP,
			ISNULL(SUM(s.Quantity * prod.Harga),0) Penjualan
		FROM dbo.Selling s
		RIGHT JOIN dbo.Outlet o
			ON o.Kode = s.KodeOutlet
		LEFT JOIN dbo.Product prod
			ON prod.Kode = s.KodeProduct
		GROUP BY
			o.Kode,
			o.Nama
	) sm
	WHERE
		sm.Penjualan = (SELECT * FROM max15) OR
		sm.Penjualan = (SELECT * FROM min15)
		--sm.Penjualan = @max OR
		--sm.Penjualan = @min

CREATE VIEW max15
AS
	SELECT
		MAX(mx.Penjualan) Penjualan
	FROM (
		SELECT
			o.Kode KodeO,
			--prod.Kode KodeP,
			SUM(s.Quantity * prod.Harga) Penjualan
		FROM dbo.Selling s
		JOIN dbo.Outlet o
			ON o.Kode = s.KodeOutlet
		JOIN dbo.Product prod
			ON prod.Kode = s.KodeProduct
		GROUP BY
			o.Kode
	) mx

ALTER VIEW min15
AS
	SELECT
		MIN(mn.Penjualan) Penjualan
	FROM (
		SELECT
			o.Kode KodeO,
			--prod.Kode KodeP,
			ISNULL(SUM(s.Quantity * prod.Harga), 0) Penjualan
		FROM dbo.Outlet o
		LEFT JOIN dbo.Selling s
			ON o.Kode = s.KodeOutlet
		LEFT JOIN dbo.Product prod
			ON prod.Kode = s.KodeProduct
		GROUP BY
			o.Kode
	) mn

SELECT MIN(Penjualan) Penjualan FROM
(SELECT
	o.Kode KodeO,
	--prod.Kode KodeP,
	ISNULL(SUM(s.Quantity * prod.Harga), 0) Penjualan
FROM dbo.Outlet o
LEFT JOIN dbo.Selling s
	ON o.Kode = s.KodeOutlet
LEFT JOIN dbo.Product prod
	ON prod.Kode = s.KodeProduct
GROUP BY
	o.Kode) MinO
--GROUP BY KodeO

