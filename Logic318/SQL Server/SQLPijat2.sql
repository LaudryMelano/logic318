CREATE DATABASE DbPijat_Pijat
GO
drop database DbPijat_Pijat
USE DbPijat_Pijat
CREATE TABLE Provinsi
(
ID INT IDENTITY(1,1) PRIMARY KEY,
Nama VARCHAR (50) NOT NULL
)
CREATE TABLE Kota 
(
ID INT IDENTITY(1,1) PRIMARY KEY ,
Nama VARCHAR(50) NOT NULL,
ID_Provinsi INT FOREIGN KEY REFERENCES Provinsi(ID)
)
CREATE TABLE Grade
(
ID INT IDENTITY(1,1) PRIMARY KEY,
[Type] VARCHAR(20) NOT NULL
)
CREATE TABLE Member
(
ID VARCHAR(50) PRIMARY KEY,
NamaDepan VARCHAR(50) NOT NULL,
NamaBelakang VARCHAR(50),
Alamat VARCHAR(200) NOT NULL,
ID_Kota INT FOREIGN KEY REFERENCES Kota (ID),
ID_Grade INT FOREIGN KEY REFERENCES Grade (ID)
)
CREATE TABLE Cashier
(
ID VARCHAR(50) PRIMARY KEY,
NamaDepan VARCHAR (50) NOT NULL,
NamaBelakang VARCHAR(50),
)
CREATE TABLE Masseuse
(
ID VARCHAR(50) PRIMARY KEY,
NamaDepan VARCHAR (50) NOT NULL,
NamaBelakang VARCHAR(50),
)
CREATE TABLE Massage
(
ID INT IDENTITY(1,1) PRIMARY KEY,
[Type] VARCHAR(20) NOT NULL,
Harga MONEY DEFAULT 0
)
CREATE TABLE Goods
(
ID INT IDENTITY(1,1) PRIMARY KEY,
[Type] VARCHAR(20) NOT NULL,
Quantity INT DEFAULT 0,
Harga MONEY DEFAULT 0
)
CREATE TABLE Booking
(
ID VARCHAR(50) PRIMARY KEY,
Tanggal DATE NOT NULL,
ID_Member VARCHAR(50) FOREIGN KEY REFERENCES Member(ID)
)

CREATE TABLE BookList
(
ID int identity(1,1),
ID_Booking VARCHAR(50) FOREIGN KEY REFERENCES Booking(ID),
ID_BookingMassage int  FOREIGN KEY REFERENCES BookingMassage(ID),
ID_BookingGoods int  FOREIGN KEY REFERENCES BookingGoods(ID),
)

insert into BookList(ID_Booking, ID_BookingMassage, ID_BookingGoods)
values('B098',null,1),('B098',null,2),('B098',null,3);

CREATE TABLE BookingMassage
(
ID INT IDENTITY(1,1) PRIMARY KEY,
ID_Booking VARCHAR(50) FOREIGN KEY REFERENCES Booking(ID),
ID_Massage INT FOREIGN KEY REFERENCES Massage(ID),
Masseuse VARCHAR(50) FOREIGN KEY REFERENCES Masseuse(ID),
Harga MONEY DEFAULT 0
)
CREATE TABLE BookingGoods
(
ID INT IDENTITY(1,1) PRIMARY KEY,
ID_Booking VARCHAR(50) FOREIGN KEY REFERENCES Booking(ID),
ID_Goods INT FOREIGN KEY REFERENCES Goods(ID),
Quantity INT NOT NULL,
Harga MONEY DEFAULT 0
)
CREATE TABLE Invoice
(
ID VARCHAR(50) PRIMARY KEY,
Tanggal DATE NOT NULL,
ID_Booking VARCHAR(50) FOREIGN KEY REFERENCES Booking(ID),
ID_Cashier VARCHAR(50) FOREIGN KEY REFERENCES Cashier(ID)
)

insert into Provinsi(Nama)
values('Banten');

insert into Kota(Nama, ID_Provinsi)
values('Tanggerang Selatan',1);

insert into Grade([type])
values('Gold');

insert into Member(ID, NamaDepan, NamaBelakang, Alamat, ID_Kota, ID_Grade)
values('M0982','Bayu',null,'Jl. Garuda',1,1);

insert into Masseuse(ID, NamaDepan, NamaBelakang)
values('MA001','Wahyu',null);

insert into Cashier(ID, NamaDepan, NamaBelakang)
values('CA001','Nino',null);

insert into Booking(ID, Tanggal, ID_Member)
values('B098','20230104','M0982');

insert into Massage([Type], Harga)
values('Full Body',120000);

insert into BookingMassage(ID_Booking, ID_Massage, Masseuse, Harga)
values('B098',1,'MA001',120000);

select * from BookingMassage
insert into Goods( Type, Quantity, Harga)
values('Miyak Terapi',60,25000),('Wedang Jahe',100,15000),
('Handuk',120,46500);

select * from BookingGoods
select * from Goods

insert into BookingGoods(ID_Goods,ID_Booking, Quantity, Harga)
values(1,'B098',1,25000),(2,'B098',1,150000),
(3,'B098',1,46500);

insert into Invoice(ID, Tanggal, ID_Booking, ID_Cashier)
values('M23010923','20230105','B098', 'CA001');

select *
from
Invoice inv
join Booking bk on inv.ID_Booking = bk.ID
join Member mbr on bk.ID_Member = mbr.ID
join Kota kt on mbr.ID_Kota = kt.ID
join Provinsi prv on kt.ID_Provinsi = prv.ID 
join Grade gr on mbr.ID_Grade = gr.ID
join Cashier cas on inv.ID_Cashier = cas.ID
join BookList bl on bk.ID = bl.ID_Booking
right join BookingMassage bm on bl.ID_BookingMassage = bm.ID
join Massage msg on bm.ID_Massage = msg.ID
join Masseuse mas on bm.Masseuse = mas.ID
right join BookingGoods bg on bl.ID_BookingGoods = bg.ID
join Goods gs on bg.ID_Goods = gs.ID

select * from BookList

select *
from
Invoice inv
join Booking bk on inv.ID_Booking = bk.ID
join Member mbr on bk.ID_Member = mbr.ID
join Kota kt on mbr.ID_Kota = kt.ID
join Provinsi prv on kt.ID_Provinsi = prv.ID 
join Grade gr on mbr.ID_Grade = gr.ID
join Cashier cas on inv.ID_Cashier = cas.ID
join BookingGoods bg on bk.ID = bg.ID_Booking
join Goods gs on bg.ID_Goods = gs.ID
