﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
    public class Array2D
    {
    }

    public class Array2DSoal01
    {
        public Array2DSoal01()
        {
            //n1=1, n2=3
            //i : 0 1 2 3   4   5   6
            //0 : 1 3 9 27  81  243 729
            Console.WriteLine("=== SOAL 01 ===");
            Console.WriteLine();
            Console.Write("Masukkan Nilai n1 : ");
            int n1 = int.Parse(Console.ReadLine());

            Console.WriteLine();

            Console.Write("Masukkan Nilai n2 : ");
            int n2 = int.Parse(Console.ReadLine());

            string[,] arr = new string[2, n1];

            for (int i = 0; i < n1; i++)
            {
                arr[0, i] = i.ToString();
                arr[1, i] = Math.Pow(n2, i).ToString();
            }

            Printing.Array2Dim(arr);
        }
    }

    public class Array2DSoal02
    {
        public Array2DSoal02()
        {
            //n1=1, n2=3
            //i : 0   1   2   3   4   5     6
            //0 : 1   3  -9  27  81 -243   729
            Console.WriteLine("=== SOAL 02 ===");
            Console.WriteLine();
            Console.Write("Masukkan Nilai n1 : ");
            int n1 = int.Parse(Console.ReadLine());

            Console.WriteLine();

            Console.Write("Masukkan Nilai n2 : ");
            int n2 = int.Parse(Console.ReadLine());

            string[,] arr = new string[2, n1];

            for (int i = 0; i < n1; i++)
            {
                arr[0, i] = i.ToString();
                if ((i + 1) % 3 == 0)
                    arr[1, i] = (Math.Pow(n2, i) * -1).ToString();
                else
                    arr[1, i] = Math.Pow(n2, i).ToString();
            }

            Printing.Array2Dim(arr);
        }
    }

    public class Array2DSoal03
    {
        public Array2DSoal03()
        {
            //n1=1, n2=3
            //i : 0   1   2     3   4   5     6
            //0 : 3	  6	  12    24	12	6	  3
            Console.WriteLine("=== SOAL 03 ===");
            Console.WriteLine();
            Console.Write("Masukkan Nilai n1 : ");
            int n1 = int.Parse(Console.ReadLine());

            Console.WriteLine();

            Console.Write("Masukkan Nilai n2 : ");
            int n2 = int.Parse(Console.ReadLine());

            string[,] arr = new string[2, n1];

            int median = n1 / 2;

            //double n3 = n2;

            for (int i = 0; i < n1; i++)
            {
                arr[0, i] = i.ToString();
                if (i <= median)
                {
                    arr[1, i] = n2.ToString();
                    arr[1, n1 - 1 - i] = n2.ToString();
                    n2 *= 2;
                }

                //if (i <= n1 / 2)
                //{
                //    arr[1, i] = n2.ToString();

                //    arr[1, n1-1-i] = n2.ToString();
                //    n2 *= 2;
                //}
            }

            Printing.Array2Dim(arr);
        }
    }

    public class Array2DSoal04
    {
        public Array2DSoal04()
        {
            //n1=7, n2=5
            //i : 0   1   2   3   4   5     6
            //0 : 1	  5	  2	  10  3	  15	4
            Console.WriteLine("=== SOAL 04 ===");
            Console.WriteLine();
            Console.Write("Masukkan Nilai n1 : ");
            int n1 = int.Parse(Console.ReadLine());

            Console.WriteLine();

            Console.Write("Masukkan Nilai n2 : ");
            int n2 = int.Parse(Console.ReadLine());

            string[,] arr = new string[2, n1];
            int count = 1;


            for (int i = 0; i < n1; i++)
            {
                arr[0, i] = i.ToString();
                if (i % 2 == 0)
                {
                    arr[1, i] = $"{count}";
                    count++;
                }
                else
                {
                    arr[1, i] = (n2 * count - 1).ToString();
                }

            }

            Printing.Array2Dim(arr);
        }
    }

    public class Array2DSoal05
    {
        public Array2DSoal05()
        {
            //i:   0   1   2   3   4   5   6
            //0 :   7   8   9   10  11  12  13
            //1 :	14  15  16  17  18  19  20

            Console.WriteLine("=== SOAL 05 ===");
            Console.WriteLine();
            Console.Write("Masukkan Nilai n : ");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine();

            string[,] arr = new string[3, n];
            //int count = n;

            //for (int i = 0; i < n; i++)
            //{
            //    arr[0, i] = i.ToString();
            //    arr[1, i] = count.ToString();
            //    arr[2, i] = (count+n).ToString();
            //    count++;
            //}
            for (int i = 0; i < n; i++)
            {
                arr[0, i] = i.ToString();
                arr[1, i] = (n + i).ToString();
                arr[2, i] = (n * 2 + i).ToString();
            }

            Printing.Array2Dim(arr);
        }
    }

    public class Array2DSoal06
    {
        public Array2DSoal06()
        {

            //0   1   2   3   4         5       6
            //1   7   49  343 2401    16807   117649
            //1   8   51  346 2405    16812   117655

            Console.WriteLine("=== SOAL 06 ===");
            Console.WriteLine();
            Console.Write("Masukkan Nilai n : ");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine();

            string[,] arr = new string[3, n];

            for (int i = 0; i < n; i++)
            {
                arr[0, i] = i.ToString();
                arr[1, i] = Math.Pow(n, i).ToString();
                arr[2, i] = (Math.Pow(n, i) + i).ToString();
            }

            Printing.Array2Dim(arr);
        }
    }

    public class Array2DSoal07
    {
        public Array2DSoal07()
        {
            //i:   0   1   2   3   4   5   6
            //0 :   7   8   9   10  11  12  13
            //1 :	14  15  16  17  18  19  20

            Console.WriteLine("=== SOAL 07 ===");
            Console.WriteLine();
            Console.Write("Masukkan Nilai n : ");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine();

            string[,] arr = new string[3, n];

            for (int i = 0; i < n; i++)
            {
                arr[0, i] = i.ToString();
                arr[1, i] = (n + i).ToString();
                arr[2, i] = (n * 2 + i).ToString();
            }

            Printing.Array2Dim(arr);
        }
    }

    public class Array2DSoal08
    {
        public Array2DSoal08()
        {
            //0   1   2   3   4   5   6
            //0   2   4   6   8   10  12
            //0   3   6   9   12  15  18

            Console.WriteLine("=== SOAL 08 ===");
            Console.WriteLine();
            Console.Write("Masukkan Nilai n : ");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine();

            string[,] arr = new string[3, n];

            for (int i = 0; i < n; i++)
            {
                arr[0, i] = i.ToString();
                arr[1, i] = (2 * i).ToString();
                arr[2, i] = (2 * i + i).ToString();
            }

            Printing.Array2Dim(arr);
        }
    }

    public class Array2DSoal09
    {
        public Array2DSoal09()
        {
            //i:   0   1   2   3   4   5   6
            //0 :   7   8   9   10  11  12  13
            //1 :	14  15  16  17  18  19  20

            Console.WriteLine("=== SOAL 09 ===");
            Console.WriteLine();
            Console.Write("Masukkan Nilai n1 : ");
            int n1 = int.Parse(Console.ReadLine());

            Console.WriteLine();

            Console.Write("Masukkan Nilai n2 : ");
            int n2 = int.Parse(Console.ReadLine());

            string[,] arr = new string[3, n1];

            for (int i = 0; i < n1; i++)
            {
                arr[0, i] = i.ToString();
                arr[1, i] = (n2 * i).ToString();
                arr[2, i] = ((n1 - 1 - i) * n2).ToString();
            }

            Printing.Array2Dim(arr);
        }
    }

    public class Array2DSoal10
    {
        public Array2DSoal10()
        {
            //i:   0   1   2   3   4   5   6
            //0 :   7   8   9   10  11  12  13
            //1 :	14  15  16  17  18  19  20

            Console.WriteLine("=== SOAL 10 ===");
            Console.WriteLine();
            Console.Write("Masukkan Nilai n1 : ");
            int n1 = int.Parse(Console.ReadLine());

            Console.WriteLine();

            Console.Write("Masukkan Nilai n2 : ");
            int n2 = int.Parse(Console.ReadLine());

            string[,] arr = new string[3, n1];

            for (int i = 0; i < n1; i++)
            {
                arr[0, i] = i.ToString();
                arr[1, i] = (n2 * i).ToString();
                arr[2, i] = (n2 * i + i).ToString();
            }

            Printing.Array2Dim(arr);
        }
    }
}
