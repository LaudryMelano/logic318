﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
    internal class Soal06
    {
        public Soal06()
        {
            Console.Write("Masukkan nilai n : ");
            int n = int.Parse(Console.ReadLine());

            string[] arrString = new string[n];
            //int count = 1;
            //for (int i = 1; i <= n; i++)
            //{
            //    if (i % 3 == 0)              
            //        Console.Write("*\t");
            //    else
            //        Console.Write($"{count}\t");
            //    count += 4;
            //}

            for (int i = 1; i <= n; i++)
            {
                //if (i % 3 == 0)
                //    Console.Write("*\t");
                //else
                //    Console.Write($"{i*4-3}\t");
                //Console.Write(i % 3 != 0 ? $"{i * 4 - 3}\t" : "*\t");
                arrString[i - 1] = i % 3 != 0 ? $"{i * 4 - 3}\t" : "*\t";
            }
            Printing.Array1Dim(arrString);
        }
    }
}
