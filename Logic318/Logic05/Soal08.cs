﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
    internal class Soal08
    {
        public Soal08()
        {
            Console.Write("Masukkan nilai n : ");
            int n = int.Parse(Console.ReadLine());

            string[] arrString = new string[n];
            //int count = 1;
            //for (int i = 0; i < n; i++)
            //{
            //    Console.Write($"{count} \t");
            //    count *= 3;
            //}

            for (int i = 0; i < n; i++)
            {
                arrString[i] = Math.Pow(3, i + 1) + "\t";
            }
            Printing.Array1Dim(arrString);
        }
    }
}
