﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
    internal class Soal04
    {
        public Soal04()
        {
            Console.Write("Masukkan nilai n : ");
            int n = int.Parse(Console.ReadLine());

            //int count = 1;
            //for (int i = 0; i < n; i++)
            //{
            //    Console.Write($"{count} \t");
            //    count += 4;
            //}

            //for (int i = 0; i < n; i++)
            //{
            //    Console.Write($"{i*4+1} \t");
            //}

            string[] arrString = new string[n];

            for (int i = 0; i < n; i++)
            {
                arrString[i] = (i * 4 + 1).ToString() + "\t";
            }
            Printing.Array1Dim(arrString);
        }
    }
}
