﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic05
{
    internal class Soal10
    {
        public Soal10()
        {
            Console.Write("Masukkan nilai n : ");
            int n = int.Parse(Console.ReadLine());

            //for (int i = 0; i < n; i++)
            //{
            //    if ((i+1)%4 == 0)
            //        Console.Write($"XXX \t");
            //    else
            //        Console.Write($"{Math.Pow(3,i+1)} \t")
            //}

            for (int i = 0; i < n; i++)
            {
                if ((i + 1) % 4 == 0)
                    Console.Write($"{DigitToString(Math.Pow(3, i + 1))}\t");
                else
                    Console.Write($"{Math.Pow(3, i + 1)} \t");
            }
        }

        private string DigitToString(double digit)
        {
            string result = "";
            for (int i = 0; i < digit.ToString().Length; i++)
            {
                result += "X";
            }

            return result;
        }
    }
}
