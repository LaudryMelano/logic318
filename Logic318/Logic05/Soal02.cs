﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
    internal class Soal02
    {
        public Soal02() 
        {
            Console.Write("Masukkan nilai n : ");
            int n = int.Parse(Console.ReadLine());

            //int count = 2;
            //for (int i = 0; i < n; i++)
            //{
            //    Console.Write($"{count} \t");
            //    count += 2;

            //    //Console.Write($"{i * 2 + 2}");
            //    //atau Console.Write($"{(i + 1) * 2}");
            //}

            string[] arrString = new string[n];

            for (int i = 0; i < n; i++)
            {
                arrString[i] = (i * 2 + 2).ToString() + "\t";
            }

            Printing.Array1Dim(arrString);
        }
    }
}
