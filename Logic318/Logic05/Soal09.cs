﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic05
{
    internal class Soal09
    {
        public Soal09()
        {
            Console.Write("Masukkan nilai n : ");
            int n = int.Parse(Console.ReadLine());

            int count = 4;
            for (int i = 1; i <= n; i++)
            {
                if (i % 3 == 0)
                {
                    Console.Write("*\t");
                }
                else
                {
                    Console.Write($"{count}\t");
                    count *= 4;
                }
            }
        }
    }
}
