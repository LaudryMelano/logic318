﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
    internal class Soal07
    {
        public Soal07()
        {
            Console.Write("Masukkan nilai n : ");
            int n = int.Parse(Console.ReadLine());

            string[] arrString = new string[n];
            //int count = 2;
            //for (int i = 0; i < n; i++)
            //{
            //    Console.Write($"{count} \t");
            //    count *= 2;
            //}

            for (int i = 0; i < n; i++)
            {
                arrString[i] = Math.Pow(2, i + 1) + "\t";
                //Console.Write($"{Math.Pow(2,i+1)} \t");
            }
            Printing.Array1Dim(arrString);
        }
    }
}
