﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
    internal class Soal05
    {
        public Soal05()
        {
            Console.Write("Masukkan nilai n : ");
            int n = int.Parse(Console.ReadLine());

            string[] arrString = new string[n];
            int count = 1;
            for (int i = 1; i <= n; i++)
            {
                if (i % 3 == 0)
                {
                    arrString[i-1] = "*\t";
                }
                else
                {
                    arrString[i-1] = $"{count}\t";
                    count += 4;
                }
            }

            //for (int i = 0; i < n; i++)
            //{
            //    arrString[i] = $"{i * 4 + 1}\t";
            //    if (i % 2 == 1)
            //    {
            //        arrString[i] = $"{i * 4 + 1}\t"+"\t*\t";
            //        n -= 1;
            //    }
            //}

            //for (int i = 1; i <= n; i++)
            //{
            //    if (i % 3 == 0)
            //        Console.Write("*\t");
            //    else if ((i % 2 == 0 || i % 2 == 1) && i > 2)
            //        Console.Write($"{i * 4 - 7}\t");
            //    else
            //        Console.Write($"{i * 4 - 3}\t");
            //}
            Printing.Array1Dim(arrString);
        }
    }
}
