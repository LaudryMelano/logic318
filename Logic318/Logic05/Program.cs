﻿// See https://aka.ms/new-console-template for more information
//Console.WriteLine("Hello, World!");

namespace Logic05
{
    public class Program
    {
        public Program()
        {
            callMenu();
        }

        static void Main(string[] args)
        {
            callMenu();   

            Console.WriteLine();
            Console.Write("Press Any Key...");
            Console.ReadKey();
        }

        static void callMenu()
        {
            bool loop = true;

            while (loop)
            {
                Console.Clear();
                Console.WriteLine();
                menu();

                Console.WriteLine();
                Console.WriteLine();
                Console.Write("Apakah anda ingin mengulangi (y/n) : ");
                string pil = Console.ReadLine().ToLower();
                if (pil == "y")
                {
                    Console.Clear();
                    Console.WriteLine();
                    menu();

                    Console.WriteLine();
                    Console.WriteLine();
                    Console.Write("Apakah anda ingin mengulangi (y/n) : ");
                    pil = Console.ReadLine().ToLower();

                    if (pil == "n")
                    {
                        loop = false;
                        Console.WriteLine();
                        Console.WriteLine("Terimakasih telah berkunjung di Logic 05");
                    }
                }
                else if (pil == "n")
                {
                    loop = false;
                    Console.WriteLine();
                    Console.WriteLine("Terimakasih telah berkunjung  di Logic 05");
                }
                else
                {
                    Console.WriteLine("Pilih Y atau N!");
                }
            }
        }

        static void menu()
        {
            Console.WriteLine("===== Welcome to Day 05 ======");
            Console.WriteLine();
            Console.Write("Masukkan no soal : ");

            int soal = int.Parse(Console.ReadLine());

            switch (soal)
            {
                case 1:
                    Soal01 soal01 = new Soal01();
                    break;
                case 2:
                    Soal02 soal02 = new Soal02();
                    break;
                case 3:
                    Soal03 soal03 = new Soal03();
                    break;
                case 4:
                    Soal04 soal04 = new Soal04();
                    break;
                case 5:
                    Soal05 soal05 = new Soal05();
                    break;
                case 6:
                    Soal06 soal06 = new Soal06();
                    break;
                case 7:
                    Soal07 soal07 = new Soal07();
                    break;
                case 8:
                    Soal08 soal08 = new Soal08();
                    break;
                case 9:
                    Soal09 soal09 = new Soal09();
                    break;
                case 10:
                    Soal10 soal10 = new Soal10();
                    break;
                case 11:
                    Array2DSoal01 array2DSoal01 = new Array2DSoal01();
                    break;
                case 12:
                    Array2DSoal02 array2DSoal02 = new Array2DSoal02();
                    break;
                case 13:
                    Array2DSoal03 array = new Array2DSoal03();
                    break;
                case 14:
                    Array2DSoal04 array2DSoal04 = new Array2DSoal04();
                    break;
                case 15:
                    Array2DSoal05 array2DSoal05 = new Array2DSoal05();
                    break;
                case 16:
                    Array2DSoal06 array2DSoal06 = new Array2DSoal06();
                    break;
                case 17:
                    Array2DSoal07 array2DSoal07 = new Array2DSoal07();
                    break;
                case 18:
                    Array2DSoal08 array2DSoal08 = new Array2DSoal08();
                    break;
                case 19:
                    Array2DSoal09 array2DSoal09 = new Array2DSoal09();
                    break;
                case 20:
                    Array2DSoal10 array2DSoal10 = new Array2DSoal10();
                    break;
                default:
                    break;
            }
        }

        static void sayHello()
        {
            Console.WriteLine("Say Heloo");
            Console.WriteLine($"Nilai {Nilai()}");
        }

        static void Penambahan()
        {
            Console.Write("Masukkan Nilai a : ");
            int a = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Nilai b : ");
            int b = int.Parse(Console.ReadLine());

            int res = Tambah(a,b);

            Console.WriteLine($"Hasil dari {a} + {b} = {res}");
        }

        static int Tambah(int a, int b)
        {
            return a + b;
        }

        static int Nilai()
        {
            return 10;
        }
    }
}
