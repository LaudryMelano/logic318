﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
    internal class Soal01
    {
        public Soal01() 
        {
            // 1    3	5	7	9	11	13
            Console.Write("Masukkan nilai n : ");
            int n = int.Parse(Console.ReadLine());

            //for (int i = 0; i < n; i++)
            //{
            //    Console.Write($"{i*2+1} \t");
            //}

            string[] arrString = new string[n];

            for (int i = 0; i < n; i++)
            {
                arrString[i] = (i * 2 + 1).ToString() + "\t";
            }

            Printing.Array1Dim(arrString);
        }
    }
}
