﻿// See https://aka.ms/new-console-template for more information

//OUTPUT
Console.WriteLine("Hello, World!");
Console.WriteLine("Hello, Laudry Melano Kaiin!");
Console.Write("Hello, Gaes");
Console.WriteLine("Hello, Laudry Melano Kaiin!");

//INPUT
Console.Write("Masukkan Nama Kamu : ");
string nama = Console.ReadLine()!;

Console.WriteLine("Hello, " + nama);
Console.WriteLine($"Hello, {nama}");
Console.WriteLine("Hello, {0}", nama);

//Variabel Eksplisit
var namaLengkap = "Laudry";
var umur = 22;



