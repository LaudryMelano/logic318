﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic09
{
    public class IceCreamParlos
    {
        public IceCreamParlos()
        {
            Console.WriteLine("====== Ice Cream Parlos ======");
            Console.WriteLine();

            Console.Write("Masukkan jumlah uang : ");
            int uang = int.Parse(Console.ReadLine());

            Console.Write("Masukkan rasa ice cream : ");
            int[] rasa = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            Console.WriteLine();
            for (int i = 0; i < rasa.Length; i++)
            {
                for (int j = i+1; j < rasa.Length; j++)
                {
                    if (rasa[i] + rasa[j]==uang)
                    {
                        Console.Write($"{i + 1}, {j + 1}");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
