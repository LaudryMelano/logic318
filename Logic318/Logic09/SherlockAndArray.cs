﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Logic09
{
    public class SherlockAndArray
    {
        public SherlockAndArray()
        {
            Console.Write("Masukkan deret angka: ");
            int[] angka = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            bool isTrue = false;
            int sumLeft = 0, sumRight = 0;
            for (int i = 0; i < angka.Length; i++)
            {
                if (i == 0)
                {
                    sumLeft = 0;
                    sumRight = Sum(angka, i + 1, angka.Length - 1);
                }
                else if (i == angka.Length - 1)
                {
                    sumRight = 0;
                    sumLeft = Sum(angka, 0, i - 1);
                }
                else
                {
                    sumLeft = Sum(angka, 0, i - 1);
                    sumRight = Sum(angka, i + 1, angka.Length - 1);
                }

                if (sumLeft == sumRight)
                {
                    isTrue = true;
                    break;
                }
                Console.WriteLine($"{sumLeft} {sumRight}");
            }
            Console.Write(isTrue ? "Yes" : "No");

            static int Sum(int[] arr, int left, int right)
            {
                int sum = 0;
                for (int i = left; i < right; i++)
                {
                    sum++;
                }

                return sum;
            }

            //int jmlhKiri = 0;
            //int jmlhKanan = arrInt.Sum();
            //string hasil = "NO";

            //for (int i = 0; i < arrInt.Length; i++)
            //{
            //    jmlhKanan -= arrInt[i];
            //    if (jmlhKiri == jmlhKanan)
            //    {
            //        hasil = "YES";
            //        break;
            //    }
            //    jmlhKiri += arrInt[i];
            //}

            //Console.WriteLine(hasil);
        }
    }
}
