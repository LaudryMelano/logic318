﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic09
{
    public class MissingNumbers
    {
        public MissingNumbers()
        {
            Console.WriteLine();
            Console.WriteLine("===== Missing Numbers ");
            Console.WriteLine();

            Console.Write("Masukkan deret 1 : ");
            int[] deret1 = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            Console.Write("Masukkan deret 2 : ");
            int[] deret2 = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            List<int> list1 = deret1.ToList();
            List<int> list2 = deret2.ToList();

            
            for (int i = 1; i < list1.Count; i++)
            {
                for (int j = i; j > 0; j--)
                    if (list1[j - 1] > list1[j])
                    {
                        int tmp = list1[j - 1];
                        list1[j - 1] = list1[j];
                        list1[j] = tmp;
                    }
            }
         
            list2.Sort();

            for (int i = 0; i < list1.Count; i++)
            {  
                    if (list2.Contains(list1[i]))
                    {
                        list2.Remove(list1[i]);
                    }
            }

            List<int> listRes = list2.Distinct().ToList();
            Console.WriteLine(String.Join(" ", listRes));

        }
    }
}
