﻿

namespace Logic09
{
    public class Program
    {
        public Program()
        {
            callMenu();
        }
        static void Main(string[] args)
        {
            callMenu();

            Console.WriteLine();
            Console.Write("Press Any Key...");
            Console.ReadKey();
        }

        static void callMenu()
        {
            bool loop = true;

            while (loop)
            {
                Console.Clear();
                Console.WriteLine();
                menu();

                Console.WriteLine();
                Console.WriteLine();
                Console.Write("Apakah anda ingin mengulangi (y/n) : ");
                string pil = Console.ReadLine().ToLower();
                if (pil == "y")
                {
                    Console.Clear();
                    Console.WriteLine();
                    menu();

                    Console.WriteLine();
                    Console.WriteLine();
                    Console.Write("Apakah anda ingin mengulangi (y/n) : ");
                    pil = Console.ReadLine().ToLower();

                    if (pil == "n")
                    {
                        loop = false;
                        Console.WriteLine();
                        Console.WriteLine("Terimakasih telah berkunjung di Logic 06");
                    }
                }
                else if (pil == "n")
                {
                    loop = false;
                    Console.WriteLine();
                    Console.WriteLine("Terimakasih telah berkunjung di Logic 06");
                }
                else
                {
                    Console.WriteLine("Pilih Y atau N!");
                }
            }
        }

        static void menu()
        {
            Console.WriteLine("===== Welcome to Logic09 ======");
            Console.WriteLine();
            Console.WriteLine("=== 1. Missing Numbers ===");
            Console.WriteLine("=== 2. SherlockAndArray ===");
            Console.WriteLine("=== 3. IceCreamParlos ===");
            Console.WriteLine("=== 4. Pairs ===");
            Console.WriteLine("=== 5. Recursive Digit Sum ===");
            Console.WriteLine("=== 6. ===");
            Console.WriteLine("=== 7. ===");

            Console.WriteLine();
            Console.Write("Masukkan no soal : ");

            int soal = int.Parse(Console.ReadLine());

            switch (soal)
            {
                case 1:
                    MissingNumbers missing = new MissingNumbers();
                    break;
                case 2:
                    SherlockAndArray sherlockAndArray = new SherlockAndArray();
                    break;
                case 3:
                    IceCreamParlos iceCreamParlos = new IceCreamParlos();
                    break;
                case 4:
                    Pairs pairs = new Pairs();
                    break;
                case 5:
                    RecursiveDigitSum recursiveDigitSum = new RecursiveDigitSum();
                    break; 
                case 6:
                    break;
                case 7:
                    PasswordCracker passwordCracker = new PasswordCracker();
                    break;
                default:
                    break;
            }
        }
    }
}
