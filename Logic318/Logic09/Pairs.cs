﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic09
{
    public class Pairs
    {
        public Pairs()
        {
            Console.WriteLine();
            Console.WriteLine("======= PAIRS =======");

            Console.Write("Masukkan angka terget : ");
            int target = int.Parse(Console.ReadLine());

            Console.Write("Masukkan deret angka : ");
            int[] arrNum = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            int count = 0;
            for (int i = 0; i < arrNum.Length; i++)
            {
                for (int j = 0; j < arrNum.Length; j++)
                {
                    if (arrNum[i] - arrNum[j]==target)
                        count++;
                }
            }

            Console.WriteLine();
            Console.WriteLine(count);
        }
    }
}
