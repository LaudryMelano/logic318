﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic09
{
    public class RecursiveDigitSum
    {
        public RecursiveDigitSum()
        {
            Console.WriteLine();
            Console.WriteLine("======= Recursive Digit Sum =======");

            Console.Write("Masukkan deret angka : ");
            int[] deret = Array.ConvertAll(Console.ReadLine().Split(""), int.Parse);

            Console.Write("Masukkan angka perulangan : ");
            int ulang = int.Parse(Console.ReadLine());
            int totalP = 0;

            for (int i = 0; i < deret.Length; i++)
            {
                totalP += deret[i];
            }

            totalP = totalP * ulang;

            char[] cek = new char[deret.Length];

            while (totalP > 9)
            {
                cek = totalP.ToString().ToCharArray();
                deret = cek.Select(c => Convert.ToInt32(c.ToString())).ToArray();       
                totalP = 0;

                for (int i = 0; i < deret.Length; i++)
                {
                    totalP += deret[i];
                }
            }

            Console.WriteLine();
            Console.WriteLine(totalP);
        }
    }
}
