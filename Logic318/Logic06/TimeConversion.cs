﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    public class TimeConversion
    {
        public TimeConversion()
        {
            Console.WriteLine("===== Time Conversion =====");

            Console.WriteLine();

            Console.Write("Masukkan Jam dalam AM/PM (07:05:45PM) : ");
            string waktu = Console.ReadLine();

            string format = waktu.Substring(8, 2);
            int jam = int.Parse(waktu.Substring(0, 2));
            int menit = int.Parse(waktu.Substring(3,2));
            int detik = int.Parse(waktu.Substring(6,2));

            if(jam <= 12 && menit<=59 && detik<=59)
            {
                if(format == "PM")
                {
                    if (jam == 12)
                    {
                        Console.WriteLine(jam.ToString() + waktu.Substring(2, 6));
                    }
                    else
                    {
                        jam += 12;
                        Console.WriteLine(jam.ToString() + waktu.Substring(2, 6));
                    }
                }
                else
                {
                    if (jam == 12)
                    {
                        Console.WriteLine("00" + waktu.Substring(2,6));
                    }
                    else
                    {
                        Console.WriteLine(jam.ToString() + waktu.Substring(2, 6));
                    }
                }
            }
            else
            {
                Console.WriteLine("Masukkan Format 12 jam AM/PM!");
            }
        }
    }
}
