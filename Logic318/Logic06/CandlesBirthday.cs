﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    public class CandlesBirthday
    {
        public CandlesBirthday()
        {
            Console.WriteLine("===== Candles Birthday ======");

            Console.WriteLine();

            Console.Write("Masukkan tinggi lilin : ");
            int[] arrCandles = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            int max = 0, count=0;
            for (int i = 0; i < arrCandles.Length; i++)
            {
                if (arrCandles[i] > max)
                    max = arrCandles[i];
                    count = 0;

                if (arrCandles[i]==max)
                    count++;
            }

            Console.WriteLine("Lilin yang tertinggi : " + max);
            Console.WriteLine("Jumlah Lilin yang tertinggi : " + count);
        }
    }
}
