﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    public class SolveMeFirst
    {
        public SolveMeFirst()
        {
            Console.WriteLine("===== Solve Me First =====");
            Console.Write("Masukkan Nilai a : ");
            int a = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Nilai b : ");
            int b = int.Parse(Console.ReadLine());

            Console.WriteLine($"{Addition(a,b)}");
        }

        public int Addition(int value1,  int value2)
        {
            return value1 + value2;
        }

        public int Addition(string value1, int value2)
        {
            return value2;
        }

        public int Addition(int value1, int value2, int value3)
        {
            return value1 + value2 + value3;
        }
    }
}
