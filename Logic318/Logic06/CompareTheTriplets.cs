﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    public class CompareTheTriplets
    {
        public CompareTheTriplets()
        {
            Console.WriteLine("===== Compare The Triplets ======");

            Console.WriteLine();

            Console.Write("Masukkan nilai Alice \t: ");
            int[] arrAlice = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);     
            
            Console.Write("Masukkan nilai Bob \t: ");
            int[] arrBob = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            int alice =0, bob =0;   
            for (int i = 0; i < arrAlice.Length; i++)
            {
                if (arrAlice[i] > arrBob[i])
                    alice++;
                else if (arrBob[i] > arrAlice[i])
                    bob++;
            }

            Console.WriteLine();
            Console.WriteLine($"Alice={alice}, Bob={bob}");
        }
    }
}
