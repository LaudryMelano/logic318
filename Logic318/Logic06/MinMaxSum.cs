﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    public class MinMaxSum
    {
        public MinMaxSum()
        {
            Console.WriteLine("===== Min Max Sum ======");

            Console.WriteLine();

            Console.Write("Masukkan deret angka : ");
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            long min = 0, max = 0; 
            int tmp = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr.Length-1; j++)
                {
                    if (arr[j] > arr[j+1])
                    {
                        tmp = arr[j];
                        arr[j] = arr[j+1];
                        arr[j+1] = tmp;
                    }
                }
            }

            for (int i = 0;i < arr.Length;i++)
            {
                if(i != 0)
                {
                    max += arr[i];
                }
                
                if(i < arr.Length - 1)
                {
                    min += arr[i];
                }
            }

            Console.WriteLine($"{min},{max}");
        }
    }
}
