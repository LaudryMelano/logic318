﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    public class SimpleArraySum
    {
        public SimpleArraySum()
        {
            Console.WriteLine("===== Simple Array Sum =====");

            Console.WriteLine();

            Console.Write("Masukkan deret angka : ");
            int[] angka = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            string tmp = "";
            int count = 0;
            for (int i = 0; i < angka.Length; i++)
            {
                tmp += angka[i];
                if (i < angka.Length-1)
                  tmp += " + ";

                count += angka[i];
            }

            Console.Write($"{tmp} = {count}");
        }
    }
}
