﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    public class DiagonalDifference
    {
        public DiagonalDifference()
        {
            Console.WriteLine("===== Diagonal Difference =====");

            Console.WriteLine();

            Console.Write("Masukkan n : ");
            int n = int.Parse(Console.ReadLine());

            int[,] arr = new int[n, n];

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.Write("Masukkan elemen Matriks [{0}],[{1}] : ", i, j);
                    arr[i, j] = int.Parse(Console.ReadLine());
                }
            }

            int diagonal1 = 0, diagonal2 = 0;
            string tmp1 = "", tmp2="";
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if (i == j)
                    {
                        diagonal1 += arr[i, j];
                        if (tmp1 == "")
                            tmp1 += arr[i, j];
                        else if (arr[i, j] < 0)
                            tmp1 += "-" + Math.Abs(arr[i, j]);
                        else
                            tmp1 += "+"+arr[i, j];
                    }
                    if (i == arr.GetLength(1)-1-j)
                    {
                        diagonal2 += arr[i, j];
                        if (tmp2 == "")
                            tmp2 += arr[i, j];
                        else if (arr[i, j] < 0)
                            tmp2 += "-" + Math.Abs(arr[i, j]);
                        else
                            tmp2 += "+" + arr[i, j];
                    }
                }
            }

            Console.WriteLine();
            Console.WriteLine("Jumlah angka diagonal pertama  : " + tmp1 + " = " + diagonal1);
            Console.WriteLine("Jumlah angka diagonal kedua    : " + tmp2 + " = " + diagonal2);
            Console.WriteLine("Jumlah perbedaan diagonal      : " + (Math.Abs(diagonal1-diagonal2)));


        }
    }
}
