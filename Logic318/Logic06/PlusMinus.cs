﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    public class PlusMinus
    {
        public PlusMinus()
        {
            Console.WriteLine("===== Plus Minus =====");
            Console.WriteLine();

            Console.Write("Masukkan deret angka : ");
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            double zero = 0, plus = 0, negative = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == 0)
                {
                    zero++;
                }else if (arr[i] > 0)
                {
                    plus++;
                }
                else
                {
                    negative++;
                }
            }

            double resPlus = plus / arr.Length;
            double resNegative = negative / arr.Length;
            double resZero = zero / arr.Length;

            Console.WriteLine(Math.Round(resPlus).ToString("n6"));
            Console.WriteLine(Math.Round(resNegative).ToString("n6"));
            Console.WriteLine(Math.Round(resZero).ToString("n6"));
        }
    }
}
