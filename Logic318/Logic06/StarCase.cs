﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    public class StarCase
    {
        public StarCase()
        {
            Console.WriteLine("====== STAR CASE =======");
            Console.Write("Masukkan jumlah bintang : ");
            int bintang = int.Parse(Console.ReadLine());

            for (int i = 0; i < bintang; i++)
            {
                for (int j = 0; j < bintang; j++)
                {
                    //if (j < bintang - 1 - i)
                    //    Console.Write(" ");
                    //else
                    //    Console.Write("#");
                    if (i + j == bintang - 1 || i == bintang - 1 || j == bintang - 1)
                        Console.Write("#");
                    else
                        Console.Write(" ");
                }
                Console.WriteLine();
            }

        }
    }
}
