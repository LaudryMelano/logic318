﻿using Logic02;

//switchCase();
//whileLoop();
//doWhile();
//nestedFor();
//forEach();
//inputArray();
//array2Dimension();
//convertArayAll();
contain();
//listAdd();

//listRemove();
//listInsert();



//initialList();
//listClass();

static void switchCase()
{
    Console.WriteLine("\t==== SWITCH CASE ====");

    Console.Write("Pilih Buah kesukaan kamu (apel/pisang/semagka) : ");
    string pilihan = Console.ReadLine().ToLower();
    Console.WriteLine();

    switch (pilihan)
    {
        case "apel":
            Console.WriteLine("Buah yang kamu dapatkan adalah apel");
            break;
        case "pisang":
            Console.WriteLine("Buah yang kamu dapatkan adalah pisang");
            break;
        case "semangka":
            Console.WriteLine("Buah yang kamu dapatkan adalah semangka");
            break;
        default:
            Console.WriteLine("Buah yang kamu mau belum terdaftar!");
            break;
    }
}

static void whileLoop()
{
    Console.WriteLine("==== WHILE LOOP ====");

    bool loop = true;
    int nilai = 1;

    while (loop)
    {
        Console.WriteLine($"Proses ke : {nilai}");

        Console.Write("Apakah anda ingin mengulangi proses (y/n)? ");
        string input = Console.ReadLine();

        if (input == "y")
        {
            nilai++;
            loop = true;
        }
        else if (input == "n")
        {
            loop = false;
        }
        else
        {
            //nilai = 1;      
            Console.WriteLine("Input yang anda masukkan salah");
            loop = true;
        }
    }
}

static void doWhile()
{
    Console.WriteLine("==== DO WHILE ====");

    Console.WriteLine();
    Console.Write("Masukkan nilai : ");
    int end = int.Parse(Console.ReadLine());

    int start = 0;
    do
    {
        Console.WriteLine(start);
        start++;
    } while (start < end);
}

static void forLoopIncrement()
{
    for(int i = 0; i < 10; i++)
    {
        Console.WriteLine(i);
    }
}

static void forLoopDecrement()
{
    for(int i=9; i>=0; i--)
    {
        Console.WriteLine(i);
    }
}

static void nestedFor()
{
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            Console.Write(" {0},{1} ", i, j);
        }
        Console.WriteLine();
    }
}

static void forEach()
{
    int[] numArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    string[] strArray = { "Laudry", "Muafa", "Marcel", "Alfi" };

    int tmp = 0;

    foreach (string str in strArray)
    {
        Console.WriteLine(str);
    }

    foreach (int item in numArray)
    {
        Console.WriteLine(item);
        tmp += item;
    }

    Console.WriteLine(tmp);
}

static void arrayFor()
{
    //Inisialisasi Array
    int[] array = { 1, 2, 3 };
    int[] array2 = new int[] { 1, 2, 3 };
    int[] array3 = new int[3];
    array[0] = 1;
    array[1] = 2;
    array[2] = 3;
    string[] strArray = { "Laudry", "Muafa", "Marcel", "Alfi" };

    for (int i = 0; i < strArray.Length; i++)
    {
        Console.WriteLine(strArray[i]);
    }

}

static void inputArray()
{
    int[] a = new int[5];

    for (int i = 0; i < a.Length; i++)
    {
        Console.Write($"Masukkan array ke - {i+1} : ");
        a[i] = int.Parse( Console.ReadLine() );
    }

    foreach(int i in a)
    {
        Console.WriteLine(i);
    }
}

static void array2Dimension()
{
    int[,] array = new int[,]
    {
        {1,2,3},
        {4,5,6},
        {7,8,9},
    };

    Console.WriteLine(array[0,2]);
    Console.WriteLine(array[1,1]);
    Console.WriteLine(array[2,1]);
    Console.WriteLine(array[2,2]);

    Console.WriteLine();

    for (int i = 0; i < array.GetLength(0); i++)
    {
        for (int j = 0; j < array.GetLength(1); j++)
        {
            Console.Write(array[i,j]);
        }
        Console.WriteLine();
    }
}

static void initialList()
{
    List<string> list = new List<string>()
    {
        "John Doe",
        "Jane Doe",
        "Joe Doe"
    };

    list.Add("James Doe");

    Console.WriteLine(string.Join(", ", list));
}

static void listClass()
{
    Console.WriteLine("==== LIST CLASS ====");
    List<User> listUser = new List<User>()
    {
        new User (){Name="John Doe", Age=23},
        new User (){Name="Jane Doe", Age=27},
        new User (){Name="Joe Doe", Age=23}
    };

    for (int i = 0; i < listUser.Count; i++)
    {
        Console.WriteLine(listUser[i].Name + " sudah berumur " + listUser[i].Age);
    }

}

static void listAdd()
{
    Console.WriteLine("==== LIST ADD ====");
    List<User> listUser = new List<User>()
    {
        new User (){Name="John Doe", Age=23},
        new User (){Name="Jane Doe", Age=27},
        new User (){Name="Joe Doe", Age=23}
    };

    User users = new User();
    users.Name = "Jason Doe";
    users.Age = 27;

    listUser.Add(new User() {Name="James Doe", Age=23});
    listUser.Add(users);

    for (int i = 0; i < listUser.Count; i++)
    {
        Console.WriteLine(listUser[i].Name + " sudah berumur " + listUser[i].Age);
    }
}

static void listRemove()
{
    Console.WriteLine("==== LIST REMOVE ====");
    List<User> listUser = new List<User>()
    {
        new User (){Name="John Doe", Age=23},
        new User (){Name="Jane Doe", Age=27},
        new User (){Name="Joe Doe", Age=23}
    };

    User users = new User();
    users.Name = "Jason Doe";
    users.Age = 27;

    listUser.Add(users);

    User user = listUser.Where(a => a.Name == "Jane Doe").FirstOrDefault()!;
    listUser.Remove(user);

    for (int i = 0; i < listUser.Count; i++)
    {
        Console.WriteLine(listUser[i].Name + " sudah berumur " + listUser[i].Age);
    }
}

static void listInsert()
{
    Console.WriteLine("==== LIST INSERT ====");

    List<User> listUser = new List<User>()
    {
        new User(){Name = "John Doe", Age = 23},
    };

    User user = new User();
    user.Name = "Jason Doe";
    user.Age = 27;

    listUser.Insert(0, new User(){ Name = "James Doe", Age = 24 });
    listUser.Insert(1, user);

    for (int i = 0; i < listUser.Count; i++)
    {
        Console.WriteLine(listUser[i].Name + " sudah berumur " + listUser[i].Age);
    }
}

static void convertArayAll()
{
    Console.WriteLine("Convert Array All");

    Console.Write("Masukkan angka array (koma) : ");
    int[] numArr = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

    Console.WriteLine(string.Join(" ", numArr));
}

static void padLeft()
{
    Console.WriteLine("==== PAD LEFT ====");

    Console.Write("Masukan Input : ");
    int input = int.Parse(Console.ReadLine());

    Console.Write("Masukkan Panjang Karakter : ");
    int length = int.Parse(Console.ReadLine());

    Console.Write("Masukkan Karakter : ");
    char chars = char.Parse(Console.ReadLine());

    Console.WriteLine($"Hasil PadLeft : {input.ToString().PadLeft(length, chars)}");
}

static void contain()
{
    Console.WriteLine("==== CONTAIN ====");

    Console.Write("Masukkan Kalimat : ");
    string kalimat = Console.ReadLine();

    Console.Write("Masukkan Contain : ");
    string contain = Console.ReadLine();

    if (kalimat.Contains(contain))
    {
        Console.WriteLine($"Kalimat dari {kalimat} mengandung " + contain);
    }
    else
    {
        Console.WriteLine($"Kalimat dari {kalimat} tidak mengandung " + contain);
    }
}